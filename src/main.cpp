#include <optional>

#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/transform.hpp>

#include "ClearRenderPass.h"
#include "CubemapRenderPass.h"
#include "FinishRenderPass.h"
#include "ModelRenderPass.h"
#include "VulkanInstance.h"
#include "VulkanRenderDevice.h"
#include "VulkanWindow.h"
#include "vulkanutils.h"

namespace {
[[nodiscard]] auto acquire_swapchain_image(VulkanRenderDevice &render_device)
    -> std::optional<uint32_t> {
  uint32_t image_index = 0;
  if (vkAcquireNextImageKHR(render_device.device, render_device.swapchain, 0,
                            render_device.image_acquired_semaphore,
                            VK_NULL_HANDLE, &image_index) != VK_SUCCESS) {
    return {};
  }
  return image_index;
}

auto present_image(VulkanRenderDevice &render_device, uint32_t image_index)
    -> void {
  const VkPresentInfoKHR present_info{
      .sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,
      .waitSemaphoreCount = 1,
      .pWaitSemaphores = &render_device.submit_complete_semaphore,
      .swapchainCount = 1,
      .pSwapchains = &render_device.swapchain,
      .pImageIndices = &image_index};
  VK_CHECK(vkQueuePresentKHR(render_device.graphics_queue, &present_info));
}

[[nodiscard]] auto compute_view_and_project_matrices()
    -> std::tuple<mat4, mat4, vec3> {
  const auto SPEED = 0.2F;
  const auto RADIUS = 10.0F;
  const vec3 camera_position_vector =
      vec3(glm::sin(static_cast<float>(glfwGetTime()) * SPEED) * RADIUS, 2.0F,
           glm::cos(static_cast<float>(glfwGetTime()) * SPEED) * RADIUS);
  const vec3 camera_target_vector = vec3(0.0F, 0.0F, 0.0F);
  const vec3 camera_up_vector = vec3(0.0F, 1.0F, 0.0F);
  const mat4 view = glm::lookAt(camera_position_vector, camera_target_vector,
                                camera_up_vector);
  const mat4 project =
      glm::perspective(glm::radians(59.0F), RATIO, 0.1F, 1000.0F);
  return {view, project, camera_position_vector};
}

auto draw(VulkanRenderDevice &render_device,
          std::vector<std::unique_ptr<IRenderPass>> &render_passes) -> void {
  // CPU waits on submit_complete_fence
  vulkanutils::wait_on_fence(render_device,
                             render_device.submit_complete_fence);

  // GPU acquires image and signals image_acquired_semaphore on completion
  auto image_index = acquire_swapchain_image(render_device);
  if (!image_index) {
    return;
  }

  // Fill commandbuffer
  vulkanutils::begin_command_buffer(render_device, *image_index);

  const auto [view, project, camera_position_vector] =
      compute_view_and_project_matrices();

  for (auto &render_pass : render_passes) {
    render_pass->update_uniform_buffer(view, project, camera_position_vector,
                                       render_device, *image_index);
    render_pass->fill_command_buffer(render_device, *image_index);
  }
  vulkanutils::end_command_buffer(render_device, *image_index);

  // GPU waits on image_acquired_semaphore, signals submit_complete_semaphore on
  // completion, signals submit_complete_fence on completion
  vulkanutils::submit_commandbuffer(render_device, *image_index);

  // GPU waits on submit_complete_semaphore
  present_image(render_device, *image_index);
}

[[nodiscard]] auto create_render_passes(VulkanRenderDevice &render_device)
    -> std::vector<std::unique_ptr<IRenderPass>> {
  std::vector<std::unique_ptr<IRenderPass>> render_passes;

  auto model_render_pass = std::make_unique<ModelRenderPass>(render_device);
  auto cubemap_render_pass = std::make_unique<CubemapRenderPass>(render_device);
  auto clear_render_pass = std::make_unique<ClearRenderPass>(render_device);
  auto finish_render_pass = std::make_unique<FinishRenderPass>(render_device);
  render_passes.emplace_back(std::move(clear_render_pass));
  render_passes.emplace_back(std::move(cubemap_render_pass));
  render_passes.emplace_back(std::move(model_render_pass));
  render_passes.emplace_back(std::move(finish_render_pass));

  return render_passes;
}
} // namespace

auto main() -> int {
  const VulkanWindow window;
  VulkanInstance instance{window.window};
  VulkanRenderDevice render_device{instance};
  auto render_passes = create_render_passes(render_device);

  while (glfwWindowShouldClose(window.window) == 0) {
    draw(render_device, render_passes);
    glfwPollEvents();
  }

  // Make sure GPU is idling before freeing resources
  VK_CHECK(vkDeviceWaitIdle(render_device.device));
}
