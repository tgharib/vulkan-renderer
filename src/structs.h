#pragma once
// ignore useless warning about fmt::print
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
#include <fmt/core.h>

#include <glm/glm.hpp>
#include <volk.h>

using glm::mat4;
using glm::vec3;
using glm::vec4;

constexpr uint32_t SCREEN_WIDTH = 1600;
constexpr uint32_t SCREEN_HEIGHT = 900;
constexpr std::string WINDOW_TITLE{"Vulkan Renderer"};
constexpr float RATIO = static_cast<float>(SCREEN_WIDTH) / SCREEN_HEIGHT;
constexpr VkClearColorValue CLEAR_COLOR_VALUE{
    .float32 = {1.0F, 1.0F, 1.0F, 1.0F}};

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define VK_CHECK(result)                                                       \
  if ((result) != VK_SUCCESS) {                                                \
    fmt::print(stderr, "Vulkan error: {} {}:{}\n", result, __FILE__,           \
               __LINE__);                                                      \
    std::quick_exit(EXIT_FAILURE);                                             \
  }
// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define BL_CHECK(value)                                                        \
  if ((value) == true) {                                                       \
    fmt::print(stderr, "Boolean error: {}:{}\n", __FILE__, __LINE__);          \
    std::quick_exit(EXIT_FAILURE);                                             \
  }

struct VulkanImage {
  VkImage image;
  VkDeviceMemory image_memory;
  VkImageView image_view;
};

struct VulkanTexture {
  uint32_t width;
  uint32_t height;
  uint32_t depth;
  VkFormat format;

  VulkanImage vulkan_image;
  VkSampler sampler;

  VkImageLayout desired_layout;
};

struct UniformBuffer {
  mat4 model_view_projection_matrix;
  mat4 model_view_matrix;
  mat4 model_matrix;
  vec4 camera_position;
};
