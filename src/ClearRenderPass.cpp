#include "ClearRenderPass.h"

#include "vulkanutils.h"

auto ClearRenderPass::update_uniform_buffer(
    mat4 view, mat4 project, vec3 camera_position_vector,
    const VulkanRenderDevice &render_device, uint32_t image_index) -> void {}

auto ClearRenderPass::fill_command_buffer(
    const VulkanRenderDevice &render_device, uint32_t image_index) -> void {
  const VkRect2D screen_rect{
      .offset = {0, 0},
      .extent = {.width = SCREEN_WIDTH, .height = SCREEN_HEIGHT}};
  const std::array<VkClearValue, 2> clear_values{
      VkClearValue{.color = CLEAR_COLOR_VALUE},
      VkClearValue{.depthStencil = {1.0F, 0}}};
  const VkRenderPassBeginInfo renderpass_info{
      .sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
      .renderPass = renderpass,
      .framebuffer = swapchain_framebuffers[image_index],
      .renderArea = screen_rect,
      .clearValueCount = static_cast<uint32_t>(clear_values.size()),
      .pClearValues = clear_values.data()};
  vkCmdBeginRenderPass(render_device.command_buffers[image_index],
                       &renderpass_info, VK_SUBPASS_CONTENTS_INLINE);
  vkCmdEndRenderPass(render_device.command_buffers[image_index]);
}

ClearRenderPass::ClearRenderPass(VulkanRenderDevice &render_device)
    : device(render_device.device) {
  vulkanutils::create_renderpass(
      render_device, renderpass,
      vulkanutils::RenderPassCreateInfo{.clear_color = true,
                                        .clear_depth = true,
                                        .flags =
                                            vulkanutils::RenderPassBit_First});
  vulkanutils::create_framebuffers(render_device, renderpass,
                                   swapchain_framebuffers);
}

ClearRenderPass::~ClearRenderPass() {
  for (auto *framebuffer : swapchain_framebuffers) {
    vulkanutils::destroy_framebuffer(device, framebuffer);
  }
  vulkanutils::destroy_renderpass(device, renderpass);
}
