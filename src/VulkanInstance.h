#pragma once
#include <functional>

#define VK_NO_PROTOTYPES
#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include "structs.h"

struct VulkanInstance {
  VkInstance instance{};
  VkSurfaceKHR surface{};
  VkDebugUtilsMessengerEXT messenger{};
  VkDebugReportCallbackEXT report_callback{};

  VulkanInstance(GLFWwindow *window);
  ~VulkanInstance();
  VulkanInstance(const VulkanInstance &) = delete;
  VulkanInstance(VulkanInstance &&) = delete;
  auto operator=(const VulkanInstance &) -> VulkanInstance & = delete;
  auto operator=(VulkanInstance &&) -> VulkanInstance & = delete;
};
