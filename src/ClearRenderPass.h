#pragma once
#include <vector>

#include "IRenderPass.h"
#include "VulkanRenderDevice.h"

struct ClearRenderPass : IRenderPass {
  ClearRenderPass(VulkanRenderDevice &render_device);
  ~ClearRenderPass() override;
  ClearRenderPass(const ClearRenderPass &) = delete;
  ClearRenderPass(ClearRenderPass &&) = delete;
  auto operator=(const ClearRenderPass &) -> ClearRenderPass & = delete;
  auto operator=(ClearRenderPass &&) -> ClearRenderPass & = delete;

  auto update_uniform_buffer(mat4 view, mat4 project,
                             vec3 camera_position_vector,
                             const VulkanRenderDevice &render_device,
                             uint32_t image_index) -> void override;
  auto fill_command_buffer(const VulkanRenderDevice &render_device,
                           uint32_t image_index) -> void override;

private:
  // vector is same size as swapchain images
  std::vector<VkFramebuffer> swapchain_framebuffers{};
  VkRenderPass renderpass{};
  VkDevice device{};
};
