/**
 * Shader taken from 3D Graphics Rendering Cookbook by Sergey Kosarevsky &
 * Viktor Latypov
 * https://github.com/PacktPublishing/3D-Graphics-Rendering-Cookbook
 */
#version 460 core

layout (location=0) in vec3 dir;

layout (location=0) out vec4 outColor;

layout (binding=1) uniform samplerCube texture1;

void main()
{
	outColor = texture(texture1, dir);
};
