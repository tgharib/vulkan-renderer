#include "VulkanInstance.h"

#include <cstdlib>

#include <fmt/core.h>

namespace {
auto create_vulkan_instance(VkInstance &out_instance) -> void {
  const std::vector<const char *> validation_layers{
      "VK_LAYER_KHRONOS_validation"};
  const std::vector<const char *> extensions{
      "VK_KHR_surface", "VK_KHR_xcb_surface", VK_EXT_DEBUG_UTILS_EXTENSION_NAME,
      VK_EXT_DEBUG_REPORT_EXTENSION_NAME,
      VK_KHR_GET_PHYSICAL_DEVICE_PROPERTIES_2_EXTENSION_NAME};
  const VkApplicationInfo application_info{
      .sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
      .pApplicationName = "Vulkan Renderer",
      .applicationVersion = VK_MAKE_VERSION(1, 0, 0),
      .pEngineName = "N/A",
      .engineVersion = VK_MAKE_VERSION(1, 0, 0),
      .apiVersion = VK_API_VERSION_1_3};
  const VkInstanceCreateInfo create_info{
      .sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
      .pApplicationInfo = &application_info,
      .enabledLayerCount = static_cast<uint32_t>(validation_layers.size()),
      .ppEnabledLayerNames = validation_layers.data(),
      .enabledExtensionCount = static_cast<uint32_t>(extensions.size()),
      .ppEnabledExtensionNames = extensions.data()};
  VK_CHECK(vkCreateInstance(&create_info, nullptr, &out_instance));

  volkLoadInstance(out_instance);
}

VKAPI_ATTR auto VKAPI_CALL
debug_callback(VkDebugUtilsMessageSeverityFlagBitsEXT /*severity*/,
               VkDebugUtilsMessageTypeFlagsEXT /*type*/,
               const VkDebugUtilsMessengerCallbackDataEXT *callback_data,
               void * /*user_data*/) -> VkBool32 {
  fmt::print("Validation layer: {}\n\n", callback_data->pMessage);
  return VK_FALSE;
}

/**
 * Function taken from 3D Graphics Rendering Cookbook by Sergey Kosarevsky &
 * Viktor Latypov
 * https://github.com/PacktPublishing/3D-Graphics-Rendering-Cookbook
 */
VKAPI_ATTR auto VKAPI_CALL debug_report_callback(
    VkDebugReportFlagsEXT flags, VkDebugReportObjectTypeEXT /*object_type*/,
    uint64_t /*object*/, size_t /*location*/, int32_t /*message_code*/,
    const char *layer_prefix, const char *message, void * /*user_data*/)
    -> VkBool32 {
  // https://github.com/zeux/niagara/blob/master/src/device.cpp   [ignoring
  // performance warnings] This silences warnings like "For optimal performance
  // image layout should be VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL instead of
  // GENERAL."
  if ((flags & VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT) != 0U) {
    return VK_FALSE;
  }

  fmt::print("Debug callback {}: {}\n\n", layer_prefix, message);
  return VK_FALSE;
}

auto init_debug_callbacks(VkInstance instance,
                          VkDebugUtilsMessengerEXT &out_messenger,
                          VkDebugReportCallbackEXT &out_report_callback)
    -> void {
  const VkDebugUtilsMessengerCreateInfoEXT messenger_create_info{
      .sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT,
      .messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT |
                         VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT,
      .messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT |
                     VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT |
                     VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT,
      .pfnUserCallback = &debug_callback};
  VK_CHECK(vkCreateDebugUtilsMessengerEXT(instance, &messenger_create_info,
                                          nullptr, &out_messenger));

  const VkDebugReportCallbackCreateInfoEXT callback_create_info{
      .sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT,
      .flags = VK_DEBUG_REPORT_WARNING_BIT_EXT |
               VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT |
               VK_DEBUG_REPORT_ERROR_BIT_EXT | VK_DEBUG_REPORT_DEBUG_BIT_EXT,
      .pfnCallback = &debug_report_callback};
  VK_CHECK(vkCreateDebugReportCallbackEXT(instance, &callback_create_info,
                                          nullptr, &out_report_callback));
}
} // namespace

VulkanInstance::VulkanInstance(GLFWwindow *window) {
  create_vulkan_instance(instance);
  init_debug_callbacks(instance, messenger, report_callback);
  BL_CHECK(glfwCreateWindowSurface(instance, window, nullptr, &surface) != 0);
}

VulkanInstance::~VulkanInstance() {
  vkDestroySurfaceKHR(instance, surface, nullptr);
  vkDestroyDebugReportCallbackEXT(instance, report_callback, nullptr);
  vkDestroyDebugUtilsMessengerEXT(instance, messenger, nullptr);
  vkDestroyInstance(instance, nullptr);
}
