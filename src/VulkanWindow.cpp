#include "VulkanWindow.h"

#include <cstdlib>

#include "structs.h"

VulkanWindow::VulkanWindow() {
  VK_CHECK(volkInitialize());
  BL_CHECK(glfwInit() == 0);
  BL_CHECK(glfwVulkanSupported() == 0);

  glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
  glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

  window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, WINDOW_TITLE.c_str(),
                            nullptr, nullptr);
  BL_CHECK(window == nullptr);

  glfwSetKeyCallback(window, [](GLFWwindow *window, int key, int /*scancode*/,
                                int action, int /*mods*/) {
    if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE)
      glfwSetWindowShouldClose(window, GLFW_TRUE);
  });
}

VulkanWindow::~VulkanWindow() {
  glfwDestroyWindow(window);
  glfwTerminate();
}
