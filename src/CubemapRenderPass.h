#pragma once
#include <vector>

#include "IRenderPass.h"
#include "VulkanRenderDevice.h"

struct CubemapRenderPass : IRenderPass {
  CubemapRenderPass(const VulkanRenderDevice &render_device);
  ~CubemapRenderPass() override;
  CubemapRenderPass(const CubemapRenderPass &) = delete;
  CubemapRenderPass(CubemapRenderPass &&) = delete;
  auto operator=(const CubemapRenderPass &) -> CubemapRenderPass & = delete;
  auto operator=(CubemapRenderPass &&) -> CubemapRenderPass & = delete;

  auto update_uniform_buffer(mat4 view, mat4 project,
                             vec3 camera_position_vector,
                             const VulkanRenderDevice &render_device,
                             uint32_t image_index) -> void override;
  auto fill_command_buffer(const VulkanRenderDevice &render_device,
                           uint32_t image_index) -> void override;

private:
  VkDescriptorSetLayout descriptor_set_layout{};
  VkDescriptorPool descriptor_pool{};
  // vector is same size as swapchain images
  std::vector<VkDescriptorSet> descriptor_sets{};

  // vector is same size as swapchain images
  std::vector<VkFramebuffer> swapchain_framebuffers{};

  VkRenderPass renderpass{};
  VkPipelineLayout pipeline_layout{};
  VkPipeline graphics_pipeline{};

  // vectors are same size as swapchain images
  std::vector<VkBuffer> uniform_buffers{};
  std::vector<VkDeviceMemory> uniform_buffers_memory{};

  VulkanTexture cubemap{};
  const VulkanRenderDevice &render_device;

  auto setup_descriptor_sets() -> void;
  auto update_descriptor_sets() -> void;
};
