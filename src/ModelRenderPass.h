#pragma once
#include <vector>

#include "IRenderPass.h"

struct ModelRenderPass : IRenderPass {
  ModelRenderPass(VulkanRenderDevice &render_device);
  ~ModelRenderPass() override;
  ModelRenderPass(const ModelRenderPass &) = delete;
  ModelRenderPass(ModelRenderPass &&) = delete;
  auto operator=(const ModelRenderPass &) -> ModelRenderPass & = delete;
  auto operator=(ModelRenderPass &&) -> ModelRenderPass & = delete;

  auto update_uniform_buffer(mat4 view, mat4 project,vec3 camera_position_vector,
                             const VulkanRenderDevice &render_device,
                             uint32_t image_index) -> void override;
  auto fill_command_buffer(const VulkanRenderDevice &render_device,
                           uint32_t image_index) -> void override;

private:
  VkDescriptorSetLayout descriptor_set_layout{};
  VkDescriptorPool descriptor_pool{};
  // TODO: consider using a vector of swapchain-related objects
  // vector is same size as swapchain images
  std::vector<VkDescriptorSet> descriptor_sets{};

  // vector is same size as swapchain images
  std::vector<VkFramebuffer> swapchain_framebuffers{};

  VkRenderPass renderpass{};
  VkPipelineLayout pipeline_layout{};
  VkPipeline graphics_pipeline{};

  // vectors are same size as swapchain images
  std::vector<VkBuffer> uniform_buffers{};
  std::vector<VkDeviceMemory> uniform_buffers_memory{};

  // Storage Buffer with index and vertex data
  size_t vertices_buffer_size{};
  size_t indices_buffer_size{};
  VkBuffer storage_buffer{};
  VkDeviceMemory storage_buffer_memory{};

  VulkanTexture texture_ao{};
  VulkanTexture texture_emissive{};
  VulkanTexture texture_albedo{};
  VulkanTexture texture_metal{};
  VulkanTexture texture_normal{};

  VulkanTexture env_map_irradiance{};
  VulkanTexture env_map{};

  VulkanTexture brdf_lut{};

  auto load_textures() -> void;
  auto setup_descriptor_sets() -> void;
  auto update_descriptor_sets() -> void;
  VulkanRenderDevice &render_device;
};
