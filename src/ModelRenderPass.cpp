#include "ModelRenderPass.h"

#include <assimp/cimport.h>
#include <assimp/postprocess.h>
#include <assimp/scene.h>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/transform.hpp>
#include <stb_image.h>

#include "vulkanutils.h"

namespace {
/**
 * Each vertex has the following data in the vertices buffer:
 * - vertex position in local space
 * - normal vector
 * - texture coordinates
 */
struct ModelVertexData {
  vec4 position;
  vec4 normal_vector;
  vec4 texture_coords;
};
auto load_vertices_buffer(const VulkanRenderDevice &render_device,
                          const char *file_name, VkBuffer &out_buffer,
                          VkDeviceMemory &out_buffer_memory,
                          size_t *out_vertices_buffer_size,
                          size_t *out_indices_buffer_size) -> void {
  const auto *scene = aiImportFile(file_name, aiProcess_Triangulate);
  BL_CHECK((scene == nullptr) || !scene->HasMeshes());

  // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
  const auto *mesh = scene->mMeshes[0];

  std::vector<ModelVertexData> vertices;
  for (unsigned i = 0; i < mesh->mNumVertices; i++) {
    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
    const auto position = mesh->mVertices[i];
    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
    const auto texture_coords = mesh->mTextureCoords[0][i];
    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
    const auto normal = mesh->mNormals[i];
    vertices.push_back({
        .position = vec4(position.x, position.y, position.z, 1.0F),
        .normal_vector = vec4(normal.x, normal.y, normal.z, 0.0F),
        .texture_coords =
            vec4(texture_coords.x, 1.0F - texture_coords.y, 0.0F, 0.0F),
    });
  }

  std::vector<unsigned int> indices;
  for (unsigned face = 0; face < mesh->mNumFaces; face++) {
    for (unsigned vertex = 0; vertex < 3; vertex++) {
      // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
      indices.push_back(mesh->mFaces[face].mIndices[vertex]);
    }
  }
  aiReleaseImport(scene);

  *out_vertices_buffer_size = vulkanutils::vector_size_in_bytes(vertices);
  *out_indices_buffer_size = vulkanutils::vector_size_in_bytes(indices);

  vulkanutils::create_vertices_buffer_from_data_pointer(
      render_device, vertices.data(), *out_vertices_buffer_size, indices.data(),
      *out_indices_buffer_size, out_buffer, out_buffer_memory);
}

auto compute_uniform_buffer(const mat4 view, const mat4 project,
                            const vec3 camera_position_vector)
    -> UniformBuffer {
  const mat4 model_scale = glm::scale(mat4(1.0F), vec3(5.0F));
  const mat4 model_pitch_rotation =
      glm::rotate(mat4(1.0F), glm::radians(-90.0F), vec3(1.0F, 0.0F, 0.0F));
  const mat4 model_yaw_rotation =
      glm::rotate(mat4(1.0F), glm::radians(180.0F), vec3(0.0F, 0.0F, 1.0F));
  const mat4 model = model_scale * model_pitch_rotation * model_yaw_rotation;

  return UniformBuffer{.model_view_projection_matrix = project * view * model,
                       .model_view_matrix = view * model,
                       .model_matrix = model,
                       .camera_position = vec4(camera_position_vector, 1.0F)};
}

auto generate_bindings() -> std::array<VkDescriptorSetLayoutBinding, 11> {
  const std::array<VkDescriptorSetLayoutBinding, 11> bindings{
      // uniform
      vulkanutils::descriptor_set_layout_binding(
          0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
          VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT),
      // vertices data
      vulkanutils::descriptor_set_layout_binding(
          1, VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, VK_SHADER_STAGE_VERTEX_BIT),
      // indices data
      vulkanutils::descriptor_set_layout_binding(
          2, VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, VK_SHADER_STAGE_VERTEX_BIT),
      // ao texture
      vulkanutils::descriptor_set_layout_binding(
          3, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
          VK_SHADER_STAGE_FRAGMENT_BIT),
      // emissive texture
      vulkanutils::descriptor_set_layout_binding(
          4, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
          VK_SHADER_STAGE_FRAGMENT_BIT),
      // albedo texture
      vulkanutils::descriptor_set_layout_binding(
          5, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
          VK_SHADER_STAGE_FRAGMENT_BIT),
      // metal texture
      vulkanutils::descriptor_set_layout_binding(
          6, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
          VK_SHADER_STAGE_FRAGMENT_BIT),
      // normal texture
      vulkanutils::descriptor_set_layout_binding(
          7, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
          VK_SHADER_STAGE_FRAGMENT_BIT),
      // env_map
      vulkanutils::descriptor_set_layout_binding(
          8, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
          VK_SHADER_STAGE_FRAGMENT_BIT),
      // env_map_irradiance
      vulkanutils::descriptor_set_layout_binding(
          9, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
          VK_SHADER_STAGE_FRAGMENT_BIT),
      // brdf_lut
      vulkanutils::descriptor_set_layout_binding(
          10, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
          VK_SHADER_STAGE_FRAGMENT_BIT)};
  return bindings;
}
} // namespace

auto ModelRenderPass::update_descriptor_sets() -> void {
  for (auto i = 0; i < render_device.swapchain_images.size(); i++) {
    const VkDescriptorBufferInfo uniform_buffer_info{
        .buffer = uniform_buffers[i],
        .offset = 0,
        .range = sizeof(UniformBuffer)};
    const VkDescriptorBufferInfo vertices_buffer_info{
        .buffer = storage_buffer, .offset = 0, .range = vertices_buffer_size};
    const VkDescriptorBufferInfo indices_buffer_info{
        .buffer = storage_buffer,
        .offset = vertices_buffer_size,
        .range = indices_buffer_size};
    const VkDescriptorImageInfo texture_ao_info{
        .sampler = texture_ao.sampler,
        .imageView = texture_ao.vulkan_image.image_view,
        .imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL};
    const VkDescriptorImageInfo texture_emissive_info{
        .sampler = texture_emissive.sampler,
        .imageView = texture_emissive.vulkan_image.image_view,
        .imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL};
    const VkDescriptorImageInfo texture_albedo_info{
        .sampler = texture_albedo.sampler,
        .imageView = texture_albedo.vulkan_image.image_view,
        .imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL};
    const VkDescriptorImageInfo texture_metal_info{
        .sampler = texture_metal.sampler,
        .imageView = texture_metal.vulkan_image.image_view,
        .imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL};
    const VkDescriptorImageInfo texture_normal_info{
        .sampler = texture_normal.sampler,
        .imageView = texture_normal.vulkan_image.image_view,
        .imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL};
    const VkDescriptorImageInfo texture_envmap_info{
        .sampler = env_map.sampler,
        .imageView = env_map.vulkan_image.image_view,
        .imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL};
    const VkDescriptorImageInfo texture_envmap_irradiance_info{
        .sampler = env_map_irradiance.sampler,
        .imageView = env_map_irradiance.vulkan_image.image_view,
        .imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL};
    const VkDescriptorImageInfo texture_brdf_info{
        .sampler = brdf_lut.sampler,
        .imageView = brdf_lut.vulkan_image.image_view,
        .imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL};
    const std::array<VkWriteDescriptorSet, 11> descriptor_writes{
        vulkanutils::write_descriptor_set(0, descriptor_sets[i],
                                          VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                                          nullptr, &uniform_buffer_info),
        vulkanutils::write_descriptor_set(1, descriptor_sets[i],
                                          VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
                                          nullptr, &vertices_buffer_info),
        vulkanutils::write_descriptor_set(2, descriptor_sets[i],
                                          VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
                                          nullptr, &indices_buffer_info),
        vulkanutils::write_descriptor_set(
            3, descriptor_sets[i], VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
            &texture_ao_info, nullptr),
        vulkanutils::write_descriptor_set(
            4, descriptor_sets[i], VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
            &texture_emissive_info, nullptr),
        vulkanutils::write_descriptor_set(
            5, descriptor_sets[i], VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
            &texture_albedo_info, nullptr),
        vulkanutils::write_descriptor_set(
            6, descriptor_sets[i], VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
            &texture_metal_info, nullptr),
        vulkanutils::write_descriptor_set(
            7, descriptor_sets[i], VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
            &texture_normal_info, nullptr),
        vulkanutils::write_descriptor_set(
            8, descriptor_sets[i], VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
            &texture_envmap_info, nullptr),
        vulkanutils::write_descriptor_set(
            9, descriptor_sets[i], VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
            &texture_envmap_irradiance_info, nullptr),
        vulkanutils::write_descriptor_set(
            10, descriptor_sets[i], VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
            &texture_brdf_info, nullptr),
    };
    vkUpdateDescriptorSets(render_device.device,
                           static_cast<uint32_t>(descriptor_writes.size()),
                           descriptor_writes.data(), 0, nullptr);
  }
}

auto ModelRenderPass::setup_descriptor_sets() -> void {
  vulkanutils::create_descriptor_set_layout(render_device, generate_bindings(),
                                            descriptor_set_layout);
  vulkanutils::allocate_descriptor_sets(render_device, descriptor_pool,
                                        descriptor_set_layout, descriptor_sets);
  update_descriptor_sets();
}

auto ModelRenderPass::load_textures() -> void {
  vulkanutils::create_vulkan_texture_from_file(
      render_device, "data/DamagedHelmet/Default_AO.jpg", texture_ao);
  vulkanutils::create_vulkan_texture_from_file(
      render_device, "data/DamagedHelmet/Default_emissive.jpg",
      texture_emissive);
  vulkanutils::create_vulkan_texture_from_file(
      render_device, "data/DamagedHelmet/Default_albedo.jpg", texture_albedo);
  vulkanutils::create_vulkan_texture_from_file(
      render_device, "data/DamagedHelmet/Default_metalRoughness.jpg",
      texture_metal);
  vulkanutils::create_vulkan_texture_from_file(
      render_device, "data/DamagedHelmet/Default_normal.jpg", texture_normal);

  vulkanutils::create_cube_vulkan_texture_from_file(
      render_device, "data/piazza_bologni_1k.hdr", env_map);
  vulkanutils::create_cube_vulkan_texture_from_file(
      render_device, "data/piazza_bologni_1k_irradiance.hdr",
      env_map_irradiance);
  vulkanutils::create_ktx_vulkan_texture_from_file(
      render_device, "data/brdfLUT.ktx", brdf_lut);
}

auto ModelRenderPass::update_uniform_buffer(
    const mat4 view, const mat4 project, const vec3 camera_position_vector,
    const VulkanRenderDevice &render_device, const uint32_t image_index)
    -> void {
  auto uniform_buffer =
      compute_uniform_buffer(view, project, camera_position_vector);
  vulkanutils::update_uniform_buffer_memory_from_data_pointer(
      render_device, image_index, &uniform_buffer, sizeof(uniform_buffer),
      uniform_buffers_memory);
}

auto ModelRenderPass::fill_command_buffer(
    const VulkanRenderDevice &render_device, uint32_t image_index) -> void {
  const VkRect2D screen_rect{
      .offset = {0, 0},
      .extent = {.width = SCREEN_WIDTH, .height = SCREEN_HEIGHT}};
  const VkRenderPassBeginInfo renderpass_info{
      .sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
      .renderPass = renderpass,
      .framebuffer = swapchain_framebuffers[image_index],
      .renderArea = screen_rect};
  vkCmdBeginRenderPass(render_device.command_buffers[image_index],
                       &renderpass_info, VK_SUBPASS_CONTENTS_INLINE);

  vkCmdBindPipeline(render_device.command_buffers[image_index],
                    VK_PIPELINE_BIND_POINT_GRAPHICS, graphics_pipeline);
  vkCmdBindDescriptorSets(render_device.command_buffers[image_index],
                          VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline_layout, 0,
                          1, &descriptor_sets[image_index], 0, nullptr);

  vkCmdDraw(render_device.command_buffers[image_index],
            static_cast<uint32_t>(indices_buffer_size / (sizeof(unsigned int))),
            1, 0, 0);

  vkCmdEndRenderPass(render_device.command_buffers[image_index]);
}

ModelRenderPass::ModelRenderPass(VulkanRenderDevice &render_device)
    : render_device(render_device) {
  load_vertices_buffer(render_device, "data/DamagedHelmet/DamagedHelmet.gltf",
                       storage_buffer, storage_buffer_memory,
                       &vertices_buffer_size, &indices_buffer_size);
  load_textures();
  vulkanutils::create_renderpass(
      render_device, renderpass,
      vulkanutils::RenderPassCreateInfo{.clear_color = false,
                                        .clear_depth = false});
  vulkanutils::create_uniform_buffers(render_device, uniform_buffers,
                                      uniform_buffers_memory);
  vulkanutils::create_framebuffers(render_device, renderpass,
                                   swapchain_framebuffers);
  vulkanutils::create_descriptor_pool(render_device, 1, 2, 8, descriptor_pool);
  setup_descriptor_sets();
  vulkanutils::create_pipeline_layout(render_device.device,
                                      descriptor_set_layout, pipeline_layout);
  vulkanutils::create_graphics_pipeline(
      render_device, renderpass, pipeline_layout,
      {"shaders/Model.vert", "shaders/Model.frag"}, graphics_pipeline);
}

ModelRenderPass::~ModelRenderPass() {
  auto *device = render_device.device;

  vulkanutils::destroy_buffer_and_memory(device, storage_buffer,
                                         storage_buffer_memory);

  for (size_t i = 0; i < uniform_buffers.size(); i++) {
    vulkanutils::destroy_buffer_and_memory(device, uniform_buffers[i],
                                           uniform_buffers_memory[i]);
  }

  vulkanutils::destroy_descriptor_set_layout(device, descriptor_set_layout);
  vulkanutils::destroy_descriptor_pool(device, descriptor_pool);

  for (auto *framebuffer : swapchain_framebuffers) {
    vulkanutils::destroy_framebuffer(device, framebuffer);
  }

  vulkanutils::destroy_renderpass(device, renderpass);
  vulkanutils::destroy_pipeline_layout(device, pipeline_layout);
  vulkanutils::destroy_graphics_pipeline(device, graphics_pipeline);

  vulkanutils::destroy_vulkan_texture(device, texture_ao);
  vulkanutils::destroy_vulkan_texture(device, texture_emissive);
  vulkanutils::destroy_vulkan_texture(device, texture_albedo);
  vulkanutils::destroy_vulkan_texture(device, texture_metal);
  vulkanutils::destroy_vulkan_texture(device, texture_normal);
  vulkanutils::destroy_vulkan_texture(device, env_map_irradiance);
  vulkanutils::destroy_vulkan_texture(device, env_map);
  vulkanutils::destroy_vulkan_texture(device, brdf_lut);
}
