#pragma once
#include "VulkanRenderDevice.h"

// NOLINTNEXTLINE(cppcoreguidelines-special-member-functions)
struct IRenderPass {
  virtual auto update_uniform_buffer(mat4 view, mat4 project,
                                     vec3 camera_position_vector,
                                     const VulkanRenderDevice &render_device,
                                     uint32_t image_index) -> void = 0;
  virtual auto fill_command_buffer(const VulkanRenderDevice &instance,
                                   uint32_t image_index) -> void = 0;
  virtual ~IRenderPass() = default;
};
