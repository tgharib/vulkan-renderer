#include "VulkanRenderDevice.h"

#include "vulkanutils.h"

namespace {
auto is_gpu_usable(VkPhysicalDevice device) -> bool {
  VkPhysicalDeviceProperties device_properties;
  vkGetPhysicalDeviceProperties(device, &device_properties);

  VkPhysicalDeviceFeatures device_features;
  vkGetPhysicalDeviceFeatures(device, &device_features);

  return device_properties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU ||
         device_properties.deviceType == VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU;
}

auto create_physical_device(VkInstance instance,
                            VkPhysicalDevice &out_physical_device) -> void {
  uint32_t device_count = 0;
  VK_CHECK(vkEnumeratePhysicalDevices(instance, &device_count, nullptr));
  BL_CHECK(device_count == 0U);

  std::vector<VkPhysicalDevice> devices(device_count);
  VK_CHECK(vkEnumeratePhysicalDevices(instance, &device_count, devices.data()));

  for (const auto &device : devices) {
    if (is_gpu_usable(device)) {
      out_physical_device = device;
      return;
    }
  }
}

auto find_graphics_queue_family(VkPhysicalDevice device) -> uint32_t {
  uint32_t family_count = 0;
  vkGetPhysicalDeviceQueueFamilyProperties(device, &family_count, nullptr);

  std::vector<VkQueueFamilyProperties> families{family_count};
  vkGetPhysicalDeviceQueueFamilyProperties(device, &family_count,
                                           families.data());

  for (uint32_t i = 0; i < families.size(); i++)
    if (families[i].queueCount > 0 &&
        ((families[i].queueFlags & VK_QUEUE_GRAPHICS_BIT) != 0U)) {
      return i;
    }

  return 0;
}

auto create_device(VkPhysicalDevice physical_device,
                   uint32_t graphics_queue_family, VkDevice &out_device)
    -> void {
  const std::vector<const char *> extensions{
      VK_KHR_SWAPCHAIN_EXTENSION_NAME,
      VK_KHR_SHADER_DRAW_PARAMETERS_EXTENSION_NAME};
  const float queue_priority = 1.0F;
  const VkDeviceQueueCreateInfo queue_create_info{
      .sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
      .queueFamilyIndex = graphics_queue_family,
      .queueCount = 1,
      .pQueuePriorities = &queue_priority};
  const VkDeviceCreateInfo create_info{
      .sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
      .queueCreateInfoCount = 1,
      .pQueueCreateInfos = &queue_create_info,
      .enabledLayerCount = 0,
      .enabledExtensionCount = static_cast<uint32_t>(extensions.size()),
      .ppEnabledExtensionNames = extensions.data(),
      .pEnabledFeatures = nullptr};
  VK_CHECK(vkCreateDevice(physical_device, &create_info, nullptr, &out_device));
}

struct SwapchainSupportDetails {
  VkSurfaceCapabilitiesKHR capabilities = {};
  std::vector<VkSurfaceFormatKHR> formats;
  std::vector<VkPresentModeKHR> present_modes;
};
auto query_swapchain_support(VkPhysicalDevice device, VkSurfaceKHR surface)
    -> SwapchainSupportDetails {
  SwapchainSupportDetails details;
  vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, surface,
                                            &details.capabilities);

  uint32_t format_count = 0;
  vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &format_count, nullptr);
  if (format_count != 0U) {
    details.formats.resize(format_count);
    vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &format_count,
                                         details.formats.data());
  }

  uint32_t present_mode_count = 0;
  vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface,
                                            &present_mode_count, nullptr);
  if (present_mode_count != 0U) {
    details.present_modes.resize(present_mode_count);
    vkGetPhysicalDeviceSurfacePresentModesKHR(
        device, surface, &present_mode_count, details.present_modes.data());
  }

  return details;
}

auto choose_swap_present_mode(
    const std::vector<VkPresentModeKHR> &available_present_modes)
    -> VkPresentModeKHR {
  for (const auto mode : available_present_modes) {
    if (mode == VK_PRESENT_MODE_MAILBOX_KHR) {
      return mode;
    }
  }
  return VK_PRESENT_MODE_FIFO_KHR;
}

auto choose_swap_image_count(const VkSurfaceCapabilitiesKHR &capabilities)
    -> uint32_t {
  const uint32_t image_count = capabilities.minImageCount + 1;

  const bool image_count_exceeded = capabilities.maxImageCount > 0 &&
                                    image_count > capabilities.maxImageCount;

  return image_count_exceeded ? capabilities.maxImageCount : image_count;
}

auto create_swapchain(VkDevice device, VkPhysicalDevice physical_device,
                      VkSurfaceKHR surface, uint32_t graphics_queue_family,
                      VkSwapchainKHR &out_swapchain) -> void {
  const VkSurfaceFormatKHR surface_format{VK_FORMAT_B8G8R8A8_UNORM,
                                          VK_COLOR_SPACE_SRGB_NONLINEAR_KHR};
  auto swapchain_support = query_swapchain_support(physical_device, surface);
  auto present_mode = choose_swap_present_mode(swapchain_support.present_modes);

  const VkSwapchainCreateInfoKHR create_info{
      .sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
      .surface = surface,
      .minImageCount = choose_swap_image_count(swapchain_support.capabilities),
      .imageFormat = surface_format.format,
      .imageColorSpace = surface_format.colorSpace,
      .imageExtent = {.width = SCREEN_WIDTH, .height = SCREEN_HEIGHT},
      .imageArrayLayers = 1,
      .imageUsage =
          VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT,
      .imageSharingMode = VK_SHARING_MODE_EXCLUSIVE,
      .queueFamilyIndexCount = 1,
      .pQueueFamilyIndices = &graphics_queue_family,
      .preTransform = swapchain_support.capabilities.currentTransform,
      .compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
      .presentMode = present_mode,
      .clipped = VK_TRUE,
      .oldSwapchain = VK_NULL_HANDLE};
  VK_CHECK(vkCreateSwapchainKHR(device, &create_info, nullptr, &out_swapchain));
}

auto create_swapchain_images(VkDevice device, VkSwapchainKHR swapchain,
                             std::vector<VkImage> &out_images,
                             std::vector<VkImageView> &out_image_views)
    -> uint32_t {
  uint32_t image_count = 0;
  VK_CHECK(vkGetSwapchainImagesKHR(device, swapchain, &image_count, nullptr));
  out_images.resize(image_count);
  out_image_views.resize(image_count);

  VK_CHECK(vkGetSwapchainImagesKHR(device, swapchain, &image_count,
                                   out_images.data()));

  for (unsigned i = 0; i < image_count; i++) {
    vulkanutils::create_image_view(
        device, out_images[i], VK_FORMAT_B8G8R8A8_UNORM,
        VK_IMAGE_ASPECT_COLOR_BIT, out_image_views[i]);
  }

  return image_count;
}

auto create_graphics_queue(VkDevice device, uint32_t graphics_queue_family,
                           VkQueue &out_graphics_queue) -> void {
  vkGetDeviceQueue(device, graphics_queue_family, 0, &out_graphics_queue);
  BL_CHECK(out_graphics_queue == nullptr);
}

auto check_for_present_support(VkPhysicalDevice physical_device,
                               uint32_t graphics_queue_family,
                               VkSurfaceKHR surface) -> void {
  VkBool32 present_supported = 0;
  vkGetPhysicalDeviceSurfaceSupportKHR(physical_device, graphics_queue_family,
                                       surface, &present_supported);
  BL_CHECK(present_supported == 0U);
}

auto create_sync_objects(VkDevice device, VkFence &submit_complete_fence,
                         VkSemaphore &image_acquired_semaphore,
                         VkSemaphore &submit_complete_semaphore) -> void {
  vulkanutils::create_fence(device, submit_complete_fence);
  vulkanutils::create_semaphore(device, image_acquired_semaphore);
  vulkanutils::create_semaphore(device, submit_complete_semaphore);
}

auto create_command_pool(VkDevice device, uint32_t graphics_queue_family,
                         VkCommandPool &out_command_pool) -> void {
  const VkCommandPoolCreateInfo create_info{
      .sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
      .flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT,
      .queueFamilyIndex = graphics_queue_family};
  VK_CHECK(
      vkCreateCommandPool(device, &create_info, nullptr, &out_command_pool));
}

auto create_depth_resources(const VulkanRenderDevice &render_device,
                            uint32_t width, uint32_t height,
                            VulkanImage &out_depth) -> void {
  const VkFormat depth_format =
      vulkanutils::find_depth_format(render_device.physical_device);

  // Create vulkan image
  vulkanutils::create_vulkan_image(render_device.device,
                                   render_device.physical_device, width, height,
                                   depth_format, out_depth);

  // Convert depth image layout to depth/stencil attachment
  vulkanutils::convert_image_layout(
      render_device, out_depth.image, depth_format, VK_IMAGE_LAYOUT_UNDEFINED,
      VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL);
}

} // namespace

VulkanRenderDevice::VulkanRenderDevice(VulkanInstance &instance) {
  create_physical_device(instance.instance, physical_device);
  auto graphics_queue_family = find_graphics_queue_family(physical_device);
  create_device(physical_device, graphics_queue_family, device);
  create_graphics_queue(device, graphics_queue_family, graphics_queue);
  check_for_present_support(physical_device, graphics_queue_family,
                            instance.surface);
  create_swapchain(device, physical_device, instance.surface,
                   graphics_queue_family, swapchain);
  auto image_count = create_swapchain_images(
      device, swapchain, swapchain_images, swapchain_image_views);
  create_sync_objects(device, submit_complete_fence, image_acquired_semaphore,
                      submit_complete_semaphore);
  create_command_pool(device, graphics_queue_family, command_pool);
  vulkanutils::create_command_buffers(device, command_pool, image_count,
                                      command_buffers);
  create_depth_resources(*this, SCREEN_WIDTH, SCREEN_HEIGHT,
                         this->depth_texture);
}

VulkanRenderDevice::~VulkanRenderDevice() {
  vkDestroySwapchainKHR(device, swapchain, nullptr);

  for (size_t i = 0; i < swapchain_images.size(); i++) {
    vulkanutils::destroy_image_view(device, swapchain_image_views[i]);
  }

  vulkanutils::destroy_vulkan_image(device, depth_texture);

  vkDestroyCommandPool(device, command_pool, nullptr);

  vkDestroySemaphore(device, image_acquired_semaphore, nullptr);
  vkDestroySemaphore(device, submit_complete_semaphore, nullptr);
  vkDestroyFence(device, submit_complete_fence, nullptr);

  vkDestroyDevice(device, nullptr);
}
