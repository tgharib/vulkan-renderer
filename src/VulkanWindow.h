#pragma once
#define VK_NO_PROTOTYPES
#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

class VulkanWindow {
public:
  GLFWwindow *window{};

  VulkanWindow();
  ~VulkanWindow();
  VulkanWindow(const VulkanWindow &) = delete;
  VulkanWindow(VulkanWindow &&) = delete;
  auto operator=(const VulkanWindow &) -> VulkanWindow & = delete;
  auto operator=(VulkanWindow &&) -> VulkanWindow & = delete;
};
