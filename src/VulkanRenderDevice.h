#pragma once

#include <vector>

#include "VulkanInstance.h"

struct VulkanRenderDevice {
  VkPhysicalDevice physical_device{};
  VkDevice device{};
  VkQueue graphics_queue{};

  VkSwapchainKHR swapchain{};
  // vectors are same size as swapchain images
  std::vector<VkImage> swapchain_images{};
  std::vector<VkImageView> swapchain_image_views{};

  VkFence submit_complete_fence{};
  VkSemaphore image_acquired_semaphore{};
  VkSemaphore submit_complete_semaphore{};

  VkCommandPool command_pool{};
  // vector is same size as swapchain images
  std::vector<VkCommandBuffer> command_buffers{};

  VulkanImage depth_texture{};

  VulkanRenderDevice(VulkanInstance &instance);
  ~VulkanRenderDevice();
  VulkanRenderDevice(const VulkanRenderDevice &) = delete;
  VulkanRenderDevice(VulkanRenderDevice &&) = delete;
  auto operator=(const VulkanRenderDevice &) -> VulkanRenderDevice & = delete;
  auto operator=(VulkanRenderDevice &&) -> VulkanRenderDevice & = delete;
};
