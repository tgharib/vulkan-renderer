﻿#pragma once

#include <cstring>
#include <vector>

#include <glm/glm.hpp>

enum BitmapType { BitmapType_2D, BitmapType_Cube };

/***
 * Class taken from 3D Graphics Rendering Cookbook by Sergey Kosarevsky &
 * Viktor Latypov
 * https://github.com/PacktPublishing/3D-Graphics-Rendering-Cookbook
 */
/// R/RG/RGB/RGBA bitmaps
struct Bitmap {
  int width = 0;
  int height = 0;
  int depth = 1;
  int num_of_channels = 3;
  int bytes_per_channel = 4;
  BitmapType type = BitmapType_2D;
  std::vector<uint8_t> data{};

  Bitmap() = default;

  Bitmap(int width, int height, int num_of_channels)
      : width(width), height(height), num_of_channels(num_of_channels),
        data(static_cast<size_t>(width * height * num_of_channels *
                                 bytes_per_channel)) {}

  Bitmap(int width, int height, int depth, int num_of_channels)
      : width(width), height(height), depth(depth),
        num_of_channels(num_of_channels),
        data(static_cast<size_t>(width * height * depth * num_of_channels *
                                 bytes_per_channel)) {}

  Bitmap(int width, int height, int num_of_channels, const void *ptr)
      : width(width), height(height), num_of_channels(num_of_channels),
        data(static_cast<size_t>(width * height * num_of_channels *
                                 bytes_per_channel)) {
    memcpy(data.data(), ptr, data.size());
  }

  inline auto set_pixel(int x_position, int y_position, const glm::vec4 &color)
      -> void {
    const int ofs = num_of_channels * (y_position * width + x_position);
    // NOLINTBEGIN
    auto *data_ = reinterpret_cast<float *>(data.data());
    if (num_of_channels > 0)
      data_[ofs + 0] = color.x;
    if (num_of_channels > 1)
      data_[ofs + 1] = color.y;
    if (num_of_channels > 2)
      data_[ofs + 2] = color.z;
    if (num_of_channels > 3)
      data_[ofs + 3] = color.w;
    // NOLINTEND
  }

  inline auto get_pixel(int x_position, int y_position) const -> glm::vec4 {
    const int ofs = num_of_channels * (y_position * width + x_position);
    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
    const auto *data_ = reinterpret_cast<const float *>(data.data());
    return {// NOLINTBEGIN
            num_of_channels > 0 ? data_[ofs + 0] : 0.0f,
            num_of_channels > 1 ? data_[ofs + 1] : 0.0F,
            num_of_channels > 2 ? data_[ofs + 2] : 0.0F,
            num_of_channels > 3 ? data_[ofs + 3] : 0.0F};
    // NOLINTEND
  }
};
