#pragma once

#include "VulkanRenderDevice.h"

/**
 * vulkanutils namespace contains:
 * 1. Pseudo-classes for some Vulkan types. Why not wrap these types with C++
 * classes? Mostly because I am trying to learn the Vulkan C API and if I wanted
 * to use C++ classes, I would use Vulkan-Hpp.
 * 2. Functions that are likely to be used by more than one class
 */
namespace vulkanutils {
/* 1. Pseudo-classes for some Vulkan types */
// VkImage and VkDeviceMemory
auto create_image_and_memory(VkDevice device, VkPhysicalDevice physical_device,
                             uint32_t width, uint32_t height, VkFormat format,
                             VkImageTiling tiling, VkImageUsageFlags usage,
                             VkMemoryPropertyFlags properties,
                             VkImage &out_image,
                             VkDeviceMemory &out_image_memory,
                             VkImageCreateFlags flags = 0) -> void;
auto destroy_image_and_memory(VkDevice device, VkImage &out_image,
                              VkDeviceMemory &out_memory) -> void;
auto create_image_from_data_pointer(const VulkanRenderDevice &render_device,
                                    VkImage &out_image,
                                    VkDeviceMemory &out_image_memory,
                                    void *data, uint32_t width, uint32_t height,
                                    VkFormat format, uint32_t layer_count = 1,
                                    VkImageCreateFlags flags = 0) -> void;
auto create_image_from_file(const VulkanRenderDevice &render_device,
                            const char *file_name, VkImage &out_image,
                            VkDeviceMemory &image_memory) -> void;
auto copy_data_pointer_to_image(const VulkanRenderDevice &render_device,
                                VkImage &out_image, uint32_t width,
                                uint32_t height, VkFormat format,
                                uint32_t layer_count, const void *data) -> void;
auto copy_buffer_to_image(const VulkanRenderDevice &render_device,
                          VkBuffer buffer, VkImage out_image, uint32_t width,
                          uint32_t height, uint32_t layer_count) -> void;
auto convert_image_layout(const VulkanRenderDevice &render_device,
                          VkImage out_image, VkFormat format,
                          VkImageLayout old_layout, VkImageLayout new_layout,
                          uint32_t layer_count = 1, uint32_t mip_levels = 1)
    -> void;
auto create_cube_image_from_file(const VulkanRenderDevice &render_device,
                                 const char *file_name, VkImage &out_image,
                                 VkDeviceMemory &image_memory) -> void;

// VkImageView
auto create_image_view(VkDevice device, VkImage image, VkFormat format,
                       VkImageAspectFlags aspect_flags,
                       VkImageView &out_image_view,
                       VkImageViewType view_type = VK_IMAGE_VIEW_TYPE_2D,
                       uint32_t layer_count = 1) -> void;
auto destroy_image_view(VkDevice device, VkImageView &out_image_view) -> void;

// VkSampler
auto create_texture_sampler(
    VkDevice device, VkSampler &out_sampler,
    VkSamplerAddressMode address_mode = VK_SAMPLER_ADDRESS_MODE_REPEAT) -> void;
auto destroy_texture_sampler(VkDevice device, VkSampler &out_sampler) -> void;

// VkPipeline
auto create_graphics_pipeline(
    const VulkanRenderDevice &render_device, VkRenderPass renderpass,
    VkPipelineLayout pipeline_layout,
    const std::vector<const char *> &shader_files, VkPipeline &out_pipeline,
    VkPrimitiveTopology topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
    bool use_depth = true, bool use_blending = true,
    bool dynamic_scissor_state = false, uint32_t num_path_control_points = 0)
    -> void;
auto destroy_graphics_pipeline(VkDevice device, VkPipeline &out_pipeline)
    -> void;

// VkPipelineLayout
auto create_pipeline_layout(VkDevice device,
                            VkDescriptorSetLayout descriptor_set_layout,
                            VkPipelineLayout &out_pipeline_layout) -> void;
auto destroy_pipeline_layout(VkDevice device,
                             VkPipelineLayout &out_pipeline_layout) -> void;

// VkBuffer and VkDeviceMemory
auto create_buffer_and_memory(VkDevice device, VkPhysicalDevice physical_device,
                              VkDeviceSize size, VkBufferUsageFlags usage,
                              VkMemoryPropertyFlags properties,
                              VkBuffer &out_buffer,
                              VkDeviceMemory &out_buffer_memory) -> void;
auto destroy_buffer_and_memory(VkDevice device, VkBuffer &out_buffer,
                               VkDeviceMemory &out_memory) -> void;
auto copy_buffer_to_buffer(const VulkanRenderDevice &render_device,
                           VkBuffer buffer, VkBuffer out_buffer,
                           VkDeviceSize size) -> void;
auto copy_data_pointer_to_buffer(const VulkanRenderDevice &render_device,
                                 const VkDeviceMemory &out_buffer_memory,
                                 VkDeviceSize device_offset, const void *data,
                                 size_t data_size) -> void;
auto create_vertices_buffer_from_data_pointer(
    const VulkanRenderDevice &render_device, const void *vertices_data,
    size_t vertices_data_size, const void *indices_data,
    size_t indices_data_size, VkBuffer &out_vertices_buffer,
    VkDeviceMemory &out_vertices_buffer_memory) -> void;

// VkRenderPass
enum RenderPassBit : uint8_t {
  RenderPassBit_First = 0x01,
  RenderPassBit_Last = 0x02,
};
struct RenderPassCreateInfo {
  bool clear_color = false;
  bool clear_depth = false;
  uint8_t flags = 0;
};
auto create_renderpass(const VulkanRenderDevice &render_device,
                       VkRenderPass &out_renderpass,
                       const RenderPassCreateInfo &create_info) -> void;
auto destroy_renderpass(VkDevice device, VkRenderPass &out_renderpass) -> void;

// VkDescriptorPool and VkDescriptorSet (VkDescriptorSets are implicitly
// destroyed when VkDescriptorPool is destroyed)
auto create_descriptor_pool(const VulkanRenderDevice &render_device,
                            uint32_t uniform_buffer_count,
                            uint32_t storage_buffer_count,
                            uint32_t sampler_count,
                            VkDescriptorPool &out_descriptor_pool) -> void;
auto destroy_descriptor_pool(VkDevice device,
                             VkDescriptorPool &out_descriptor_pool) -> void;
auto allocate_descriptor_sets(
    const VulkanRenderDevice &render_device,
    const VkDescriptorPool &descriptor_pool,
    const VkDescriptorSetLayout &descriptor_set_layout,
    std::vector<VkDescriptorSet> &out_descriptor_sets) -> void;

// VkDescriptorSetLayout
template <std::size_t SIZE>
auto create_descriptor_set_layout(
    const VulkanRenderDevice &render_device,
    const std::array<VkDescriptorSetLayoutBinding, SIZE> &bindings,
    VkDescriptorSetLayout &out_descriptor_set_layout) -> void {
  const VkDescriptorSetLayoutCreateInfo layout_info{
      .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
      .bindingCount = static_cast<uint32_t>(bindings.size()),
      .pBindings = bindings.data()};
  VK_CHECK(vkCreateDescriptorSetLayout(render_device.device, &layout_info,
                                       nullptr, &out_descriptor_set_layout));
}
auto destroy_descriptor_set_layout(
    VkDevice device, VkDescriptorSetLayout &out_descriptor_set_layout) -> void;

// VkFramebuffer
auto create_framebuffers(const VulkanRenderDevice &render_device,
                         VkRenderPass renderpass,
                         std::vector<VkFramebuffer> &out_swapchain_framebuffers)
    -> void;
auto destroy_framebuffer(VkDevice device, VkFramebuffer &out_framebuffer)
    -> void;

// VkCommandBuffer
auto create_command_buffers(VkDevice device, VkCommandPool command_pool,
                            uint32_t image_count,
                            std::vector<VkCommandBuffer> &out_command_buffers)
    -> void;
auto begin_command_buffer(VulkanRenderDevice &render_device,
                          uint32_t image_index) -> void;
auto end_command_buffer(VulkanRenderDevice &render_device, uint32_t image_index)
    -> void;
auto submit_commandbuffer(VulkanRenderDevice &render_device,
                          uint32_t image_index) -> void;
[[nodiscard]] auto
begin_single_use_command_buffer(const VulkanRenderDevice &render_device)
    -> VkCommandBuffer;
auto end_single_use_command_buffer(const VulkanRenderDevice &render_device,
                                   VkCommandBuffer command_buffer) -> void;

// VkSemaphore and VkFence
auto create_semaphore(VkDevice device, VkSemaphore &out_semaphore) -> void;
auto create_fence(VkDevice device, VkFence &out_fence) -> void;
auto wait_on_fence(VulkanRenderDevice &render_device, VkFence fence) -> void;

// VulkanTexture
auto create_vulkan_texture_from_file(const VulkanRenderDevice &render_device,
                                     const char *file_path,
                                     VulkanTexture &out_texture) -> void;
auto create_cube_vulkan_texture_from_file(
    const VulkanRenderDevice &render_device, const char *file_path,
    VulkanTexture &out_texture) -> void;
auto create_ktx_vulkan_texture_from_file(
    const VulkanRenderDevice &render_device, const char *file_path,
    VulkanTexture &out_texture) -> void;
auto destroy_vulkan_texture(VkDevice device, VulkanTexture &texture) -> void;

// VulkanImage
auto create_vulkan_image(VkDevice device, VkPhysicalDevice physical_device,
                         uint32_t width, uint32_t height, VkFormat depth_format,
                         VulkanImage &out_vulkan_image) -> void;
auto destroy_vulkan_image(VkDevice device, VulkanImage &out_image) -> void;

// Vector of uniform buffers
auto create_uniform_buffers(
    const VulkanRenderDevice &render_device,
    std::vector<VkBuffer> &out_uniform_buffers,
    std::vector<VkDeviceMemory> &out_uniform_buffers_memory) -> void;
auto update_uniform_buffer_memory_from_data_pointer(
    const VulkanRenderDevice &render_device, size_t image_index,
    const void *data, size_t data_size,
    std::vector<VkDeviceMemory> &out_uniform_buffers_memory) -> void;

/* 2. Functions that are likely to be used by more than one class */

[[nodiscard]] auto find_depth_format(VkPhysicalDevice device) -> VkFormat;

// Returns size of std::vector<T>'s data in bytes
template <typename T>
[[nodiscard]] auto vector_size_in_bytes(const std::vector<T> &vector)
    -> size_t {
  return static_cast<size_t>(sizeof(T) * vector.size());
}

// Clamp value to [lower_bound, upper_bound]
template <typename T>
[[nodiscard]] auto clamp(T value, T lower_bound, T upper_bound) -> T {
  if (value < lower_bound) {
    return lower_bound;
  }
  if (value > upper_bound) {
    return upper_bound;
  }
  return value;
}

[[nodiscard]] inline auto descriptor_set_layout_binding(
    uint32_t binding, VkDescriptorType descriptor_type,
    VkShaderStageFlags stage_flags, uint32_t descriptor_count = 1)
    -> VkDescriptorSetLayoutBinding {
  return VkDescriptorSetLayoutBinding{.binding = binding,
                                      .descriptorType = descriptor_type,
                                      .descriptorCount = descriptor_count,
                                      .stageFlags = stage_flags,
                                      .pImmutableSamplers = nullptr};
}

[[nodiscard]] inline auto
write_descriptor_set(uint32_t index, VkDescriptorSet descriptor_set,
                     VkDescriptorType descriptor_type,
                     const VkDescriptorImageInfo *descriptor_image_info,
                     const VkDescriptorBufferInfo *descriptor_buffer_info)
    -> VkWriteDescriptorSet {
  return VkWriteDescriptorSet{.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
                              .dstSet = descriptor_set,
                              .dstBinding = index,
                              .dstArrayElement = 0,
                              .descriptorCount = 1,
                              .descriptorType = descriptor_type,
                              .pImageInfo = descriptor_image_info,
                              .pBufferInfo = descriptor_buffer_info};
}
} // namespace vulkanutils
