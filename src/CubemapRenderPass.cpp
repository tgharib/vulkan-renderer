#include "CubemapRenderPass.h"

#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/transform.hpp>

#include "vulkanutils.h"

namespace {
auto compute_uniform_buffer(const mat4 view, const mat4 project)
    -> UniformBuffer {
  const mat4 cubemap_flip =
      glm::rotate(mat4(1.0F), glm::radians(180.0F), vec3(1.0F, 0.0F, 0.0F));
  const mat4 cubemap_scale = glm::scale(mat4(1.0F), vec3(50.0F));
  const mat4 model = cubemap_scale * cubemap_flip;

  return UniformBuffer{.model_view_projection_matrix = project * view * model};
}

auto generate_bindings() -> std::array<VkDescriptorSetLayoutBinding, 2> {
  const std::array<VkDescriptorSetLayoutBinding, 2> bindings{
      // uniform
      vulkanutils::descriptor_set_layout_binding(
          0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, VK_SHADER_STAGE_VERTEX_BIT),
      // texture for fragment shader
      vulkanutils::descriptor_set_layout_binding(
          1, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
          VK_SHADER_STAGE_FRAGMENT_BIT)};
  return bindings;
}
} // namespace

auto CubemapRenderPass::update_descriptor_sets() -> void {
  for (auto i = 0; i < render_device.swapchain_images.size(); i++) {
    const VkDescriptorBufferInfo uniform_buffer_info{
        .buffer = uniform_buffers[i],
        .offset = 0,
        .range = sizeof(UniformBuffer)};
    const VkDescriptorImageInfo texture_image_info{
        .sampler = cubemap.sampler,
        .imageView = cubemap.vulkan_image.image_view,
        .imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL};

    const std::array<VkWriteDescriptorSet, 2> descriptor_writes{
        vulkanutils::write_descriptor_set(0, descriptor_sets[i],
                                          VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                                          nullptr, &uniform_buffer_info),
        vulkanutils::write_descriptor_set(
            1, descriptor_sets[i], VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
            &texture_image_info, nullptr)};

    vkUpdateDescriptorSets(render_device.device,
                           static_cast<uint32_t>(descriptor_writes.size()),
                           descriptor_writes.data(), 0, nullptr);
  }
}

auto CubemapRenderPass::setup_descriptor_sets() -> void {
  vulkanutils::create_descriptor_set_layout(render_device, generate_bindings(),
                                            descriptor_set_layout);
  vulkanutils::allocate_descriptor_sets(render_device, descriptor_pool,
                                        descriptor_set_layout, descriptor_sets);
  update_descriptor_sets();
}

auto CubemapRenderPass::update_uniform_buffer(
    const mat4 view, const mat4 project, vec3 /*camera_position_vector*/,
    const VulkanRenderDevice &render_device, uint32_t image_index) -> void {
  auto uniform_buffer = compute_uniform_buffer(view, project);
  vulkanutils::update_uniform_buffer_memory_from_data_pointer(
      render_device, image_index, &uniform_buffer, sizeof(uniform_buffer),
      uniform_buffers_memory);
}

auto CubemapRenderPass::fill_command_buffer(
    const VulkanRenderDevice &render_device, uint32_t image_index) -> void {
  const VkRect2D screen_rect{
      .offset = {0, 0},
      .extent = {.width = SCREEN_WIDTH, .height = SCREEN_HEIGHT}};
  const VkRenderPassBeginInfo renderpass_info{
      .sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
      .renderPass = renderpass,
      .framebuffer = swapchain_framebuffers[image_index],
      .renderArea = screen_rect};
  vkCmdBeginRenderPass(render_device.command_buffers[image_index],
                       &renderpass_info, VK_SUBPASS_CONTENTS_INLINE);

  vkCmdBindPipeline(render_device.command_buffers[image_index],
                    VK_PIPELINE_BIND_POINT_GRAPHICS, graphics_pipeline);
  vkCmdBindDescriptorSets(render_device.command_buffers[image_index],
                          VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline_layout, 0,
                          1, &descriptor_sets[image_index], 0, nullptr);

  vkCmdDraw(render_device.command_buffers[image_index], 36, 1, 0, 0);

  vkCmdEndRenderPass(render_device.command_buffers[image_index]);
}

CubemapRenderPass::CubemapRenderPass(const VulkanRenderDevice &render_device)
    : render_device(render_device) {
  vulkanutils::create_cube_vulkan_texture_from_file(
      render_device, "data/piazza_bologni_1k.hdr", cubemap);
  vulkanutils::create_renderpass(
      render_device, renderpass,
      vulkanutils::RenderPassCreateInfo{.clear_color = false,
                                        .clear_depth = false});
  vulkanutils::create_uniform_buffers(render_device, uniform_buffers,
                                      uniform_buffers_memory);
  vulkanutils::create_framebuffers(render_device, renderpass,
                                   swapchain_framebuffers);
  vulkanutils::create_descriptor_pool(render_device, 1, 0, 1, descriptor_pool);
  setup_descriptor_sets();
  vulkanutils::create_pipeline_layout(render_device.device,
                                      descriptor_set_layout, pipeline_layout);
  vulkanutils::create_graphics_pipeline(
      render_device, renderpass, pipeline_layout,
      {"shaders/Cubemap.vert", "shaders/Cubemap.frag"}, graphics_pipeline);
}

CubemapRenderPass::~CubemapRenderPass() {
  auto *device = render_device.device;

  for (size_t i = 0; i < uniform_buffers.size(); i++) {
    vulkanutils::destroy_buffer_and_memory(device, uniform_buffers[i],
                                           uniform_buffers_memory[i]);
  }
  vulkanutils::destroy_descriptor_set_layout(device, descriptor_set_layout);
  vulkanutils::destroy_descriptor_pool(device, descriptor_pool);
  for (auto *framebuffer : swapchain_framebuffers) {
    vulkanutils::destroy_framebuffer(device, framebuffer);
  }
  vulkanutils::destroy_texture_sampler(device, cubemap.sampler);
  vulkanutils::destroy_vulkan_image(device, cubemap.vulkan_image);
  vulkanutils::destroy_renderpass(device, renderpass);
  vulkanutils::destroy_pipeline_layout(device, pipeline_layout);
  vulkanutils::destroy_graphics_pipeline(device, graphics_pipeline);
}
