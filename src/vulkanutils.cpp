#include "vulkanutils.h"

#include <cstddef>
#include <fstream>

#include <gli/load_ktx.hpp>
#include <stb_image.h>

#include "Bitmap.h"
#include "VulkanRenderDevice.h"

using glm::ivec2;
using glm::vec2;
using glm::vec3;
using glm::vec4;

namespace vulkanutils {
namespace {
auto ends_with(std::string_view string, const char *part) -> bool {
  return string.ends_with(part);
}

auto get_shader_stage_flag(const char *fileName) -> VkShaderStageFlagBits {
  if (ends_with(fileName, ".vert")) {
    return VK_SHADER_STAGE_VERTEX_BIT;
  }

  if (ends_with(fileName, ".frag")) {
    return VK_SHADER_STAGE_FRAGMENT_BIT;
  }

  if (ends_with(fileName, ".geom")) {
    return VK_SHADER_STAGE_GEOMETRY_BIT;
  }

  if (ends_with(fileName, ".comp")) {
    return VK_SHADER_STAGE_COMPUTE_BIT;
  }

  if (ends_with(fileName, ".tesc")) {
    return VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT;
  }

  if (ends_with(fileName, ".tese")) {
    return VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT;
  }

  return VK_SHADER_STAGE_VERTEX_BIT;
}

auto read_file(const std::string &filename) -> std::vector<char> {
  std::ifstream file(filename, std::ios::ate | std::ios::binary);
  BL_CHECK(!file.is_open());

  auto file_size = file.tellg();
  std::vector<char> buffer(file_size);

  file.seekg(0);
  file.read(buffer.data(), static_cast<std::streamsize>(file_size));

  file.close();
  return buffer;
}

auto create_shader_module(VkDevice device, VkShaderModule &out_shader,
                          const char *file_name) -> VkResult {
  auto spirv_data = read_file(file_name);
  const VkShaderModuleCreateInfo createInfo{
      .sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
      .codeSize = spirv_data.size(),
      // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
      .pCode = reinterpret_cast<const uint32_t *>(spirv_data.data())};
  return vkCreateShaderModule(device, &createInfo, nullptr, &out_shader);
}

auto find_supported_format(VkPhysicalDevice device,
                           const std::vector<VkFormat> &candidates,
                           VkImageTiling tiling, VkFormatFeatureFlags features)
    -> VkFormat {
  for (const VkFormat format : candidates) {
    VkFormatProperties props;
    vkGetPhysicalDeviceFormatProperties(device, format, &props);

    if (tiling == VK_IMAGE_TILING_LINEAR &&
        (props.linearTilingFeatures & features) == features) {
      return format;
    }
    if (tiling == VK_IMAGE_TILING_OPTIMAL &&
        (props.optimalTilingFeatures & features) == features) {
      return format;
    }
  }

  BL_CHECK(true);
  return VK_FORMAT_UNDEFINED;
}

auto find_memory_type(VkPhysicalDevice device, uint32_t typeFilter,
                      VkMemoryPropertyFlags properties) -> uint32_t {
  VkPhysicalDeviceMemoryProperties mem_properties;
  vkGetPhysicalDeviceMemoryProperties(device, &mem_properties);

  for (uint32_t i = 0; i < mem_properties.memoryTypeCount; i++) {
    if (((typeFilter & (1 << i)) != 0U) &&
        // NOLINTNEXTLINE
        (mem_properties.memoryTypes[i].propertyFlags & properties) ==
            properties) {
      return i;
    }
  }

  return 0xFFFFFFFF;
}

auto has_stencil_component(VkFormat format) -> bool {
  return format == VK_FORMAT_D32_SFLOAT_S8_UINT ||
         format == VK_FORMAT_D24_UNORM_S8_UINT;
}

// NOLINTNEXTLINE(readability-function-cognitive-complexity)
auto convert_image_layout_command(VkCommandBuffer command_buffer, VkImage image,
                                  VkFormat format, VkImageLayout old_layout,
                                  VkImageLayout new_layout,
                                  uint32_t layer_count, uint32_t mip_levels)
    -> void {
  VkImageMemoryBarrier barrier{.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
                               .srcAccessMask = 0,
                               .dstAccessMask = 0,
                               .oldLayout = old_layout,
                               .newLayout = new_layout,
                               .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
                               .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
                               .image = image,
                               .subresourceRange = VkImageSubresourceRange{
                                   .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
                                   .baseMipLevel = 0,
                                   .levelCount = mip_levels,
                                   .baseArrayLayer = 0,
                                   .layerCount = layer_count}};

  VkPipelineStageFlags source_stage{};
  VkPipelineStageFlags destination_stage{};

  // if depth/stencil buffer, barrier will work on that. otherwise, it's a color
  // buffer.
  if (new_layout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL ||
      (format == VK_FORMAT_D16_UNORM) ||
      (format == VK_FORMAT_X8_D24_UNORM_PACK32) ||
      (format == VK_FORMAT_D32_SFLOAT) || (format == VK_FORMAT_S8_UINT) ||
      (format == VK_FORMAT_D16_UNORM_S8_UINT) ||
      (format == VK_FORMAT_D24_UNORM_S8_UINT)) {
    barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;

    if (has_stencil_component(format)) {
      barrier.subresourceRange.aspectMask |= VK_IMAGE_ASPECT_STENCIL_BIT;
    }
  } else {
    barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
  }

  // convert to shader read-only: barrier before fragment shader
  if (old_layout == VK_IMAGE_LAYOUT_UNDEFINED &&
      new_layout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL) {
    barrier.srcAccessMask = 0;
    barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

    source_stage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
    destination_stage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
    // convert to general: barrier after copies and before fragment shader
  } else if (old_layout == VK_IMAGE_LAYOUT_UNDEFINED &&
             new_layout == VK_IMAGE_LAYOUT_GENERAL) {
    barrier.srcAccessMask = 0;
    barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

    source_stage = VK_PIPELINE_STAGE_TRANSFER_BIT;
    destination_stage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
  }

  // convert to updateable: barrier before copies
  if (old_layout == VK_IMAGE_LAYOUT_UNDEFINED &&
      new_layout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL) {
    barrier.srcAccessMask = 0;
    barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;

    source_stage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
    destination_stage = VK_PIPELINE_STAGE_TRANSFER_BIT;
  }
  // convert from shader read-only to updateable: barrier after fragment shader
  // and before copies
  else if (old_layout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL &&
           new_layout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL) {
    barrier.srcAccessMask = VK_ACCESS_SHADER_READ_BIT;
    barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;

    source_stage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
    destination_stage = VK_PIPELINE_STAGE_TRANSFER_BIT;
  }
  // convert from updateable to shader read-only: barrier after copies and
  // before fragment shader
  else if (old_layout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL &&
           new_layout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL) {
    barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
    barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

    source_stage = VK_PIPELINE_STAGE_TRANSFER_BIT;
    destination_stage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
  }
  // convert depth texture from undefined state to depth-stencil buffer: barrier
  // before fragment shader
  else if (old_layout == VK_IMAGE_LAYOUT_UNDEFINED &&
           new_layout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL) {
    barrier.srcAccessMask = 0;
    barrier.dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT |
                            VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;

    source_stage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
    destination_stage = VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
  }
  // wait for render pass to complete
  else if (old_layout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL &&
           new_layout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL) {
    barrier.srcAccessMask = 0;
    barrier.dstAccessMask = 0;
    source_stage = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    destination_stage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
  }
  // convert from shader read-only to color attachment
  else if (old_layout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL &&
           new_layout == VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL) {
    barrier.srcAccessMask = VK_ACCESS_SHADER_READ_BIT;
    barrier.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

    source_stage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
    destination_stage = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
  }
  /* Convert from updateable texture to shader read-only */
  else if (old_layout == VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL &&
           new_layout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL) {
    barrier.srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
    barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

    source_stage = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    destination_stage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
  }
  /* Convert back from read-only to depth attachment */
  else if (old_layout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL &&
           new_layout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL) {
    barrier.srcAccessMask = VK_ACCESS_SHADER_READ_BIT;
    barrier.dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;

    source_stage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
    destination_stage = VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT;
  }
  /* Convert from updateable depth texture to shader read-only */
  else if (old_layout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL &&
           new_layout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL) {
    barrier.srcAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
    barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

    source_stage = VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT;
    destination_stage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
  }

  vkCmdPipelineBarrier(command_buffer, source_stage, destination_stage, 0, 0,
                       nullptr, 0, nullptr, 1, &barrier);
}

auto bytes_per_texture_format(VkFormat fmt) -> uint32_t {
  switch (fmt) {
  case VK_FORMAT_R8_SINT:
  case VK_FORMAT_R8_UNORM:
    return 1;
  case VK_FORMAT_R16_SFLOAT:
    return 2;
  case VK_FORMAT_R16G16_SFLOAT:
  case VK_FORMAT_R16G16_SNORM:
  case VK_FORMAT_B8G8R8A8_UNORM:
  case VK_FORMAT_R8G8B8A8_UNORM:
    return 4;
  case VK_FORMAT_R16G16B16A16_SFLOAT:
    return 4 * sizeof(uint16_t);
  case VK_FORMAT_R32G32B32A32_SFLOAT:
    return 4 * sizeof(float);
  default:
    break;
  }
  return 0;
}

/***
 * Function taken from 3D Graphics Rendering Cookbook by Sergey Kosarevsky &
 * Viktor Latypov
 * https://github.com/PacktPublishing/3D-Graphics-Rendering-Cookbook
 */
auto float24to32(int width, int height, const float *img24, float *img32)
    -> void {
  const int numPixels = width * height;
  for (int i = 0; i != numPixels; i++) {
    // NOLINTBEGIN
    *img32++ = *img24++;
    *img32++ = *img24++;
    *img32++ = *img24++;
    *img32++ = 1.0F;
    // NOLINTEND
  }
}

/***
 * Function taken from 3D Graphics Rendering Cookbook by Sergey Kosarevsky &
 * Viktor Latypov
 * https://github.com/PacktPublishing/3D-Graphics-Rendering-Cookbook
 */
// NOLINTNEXTLINE(readability-identifier-length)
auto face_coords_to_XYZ(int i, int j, int faceID, int face_size) -> vec3 {
  // NOLINTNEXTLINE(readability-identifier-length)
  const float A = 2.0F * static_cast<float>(i) / static_cast<float>(face_size);
  // NOLINTNEXTLINE(readability-identifier-length)
  const float B = 2.0F * static_cast<float>(j) / static_cast<float>(face_size);

  if (faceID == 0)
    return {-1.0F, A - 1.0F, B - 1.0F};
  if (faceID == 1)
    return {A - 1.0F, -1.0F, 1.0F - B};
  if (faceID == 2)
    return {1.0F, A - 1.0F, 1.0F - B};
  if (faceID == 3)
    return {1.0F - A, 1.0F, 1.0F - B};
  if (faceID == 4)
    return {B - 1.0F, A - 1.0F, 1.0F};
  if (faceID == 5)
    return {1.0F - B, A - 1.0F, -1.0F};

  return {};
}

/***
 * Function taken from 3D Graphics Rendering Cookbook by Sergey Kosarevsky &
 * Viktor Latypov
 * https://github.com/PacktPublishing/3D-Graphics-Rendering-Cookbook
 */
auto convertEquirectangularMapToVerticalCross(const Bitmap &bitmap) -> Bitmap {
  // NOLINTBEGIN
  if (bitmap.type != BitmapType_2D)
    return {};

  const int face_size = bitmap.width / 4;

  const int width = face_size * 3;
  const int height = face_size * 4;

  Bitmap result(width, height, bitmap.num_of_channels);

  const ivec2 FACE_OFFSETS[] = {ivec2(face_size, face_size * 3),
                                ivec2(0, face_size),
                                ivec2(face_size, face_size),
                                ivec2(face_size * 2, face_size),
                                ivec2(face_size, 0),
                                ivec2(face_size, face_size * 2)};

  const int clampW = bitmap.width - 1;
  const int clampH = bitmap.height - 1;

  for (int face = 0; face < 6; face++) {
    for (int i = 0; i < face_size; i++) {
      for (int j = 0; j < face_size; j++) {
        const vec3 P = face_coords_to_XYZ(i, j, face, face_size);
        const float R = hypot(P.x, P.y);
        const float theta = atan2(P.y, P.x);
        const float phi = atan2(P.z, R);
        //	float point source coordinates
        const auto Uf = float(2.0F * face_size * (theta + M_PI) / M_PI);
        const auto Vf = float(2.0F * face_size * (M_PI / 2.0F - phi) / M_PI);
        // 4-samples for bilinear interpolation
        const int U1 = clamp(int(floor(Uf)), 0, clampW);
        const int V1 = clamp(int(floor(Vf)), 0, clampH);
        const int U2 = clamp(U1 + 1, 0, clampW);
        const int V2 = clamp(V1 + 1, 0, clampH);
        // fractional part
        const float s = Uf - U1;
        const float t = Vf - V1;
        // fetch 4-samples
        const vec4 A = bitmap.get_pixel(U1, V1);
        const vec4 B = bitmap.get_pixel(U2, V1);
        const vec4 C = bitmap.get_pixel(U1, V2);
        const vec4 D = bitmap.get_pixel(U2, V2);
        // bilinear interpolation
        const vec4 color = A * (1 - s) * (1 - t) + B * (s) * (1 - t) +
                           C * (1 - s) * t + D * (s) * (t);
        result.set_pixel(i + FACE_OFFSETS[face].x, j + FACE_OFFSETS[face].y,
                         color);
      }
    };
  }

  return result;
  // NOLINTEND
}

/***
 * Function taken from 3D Graphics Rendering Cookbook by Sergey Kosarevsky &
 * Viktor Latypov
 * https://github.com/PacktPublishing/3D-Graphics-Rendering-Cookbook
 */
auto convertVerticalCrossToCubeMapFaces(const Bitmap &bitmap) -> Bitmap {
  const int face_width = bitmap.width / 3;
  const int face_height = bitmap.height / 4;

  Bitmap cubemap(face_width, face_height, 6, bitmap.num_of_channels);
  cubemap.type = BitmapType_Cube;

  const uint8_t *source = bitmap.data.data();
  uint8_t *destination = cubemap.data.data();

  /*
                  ------
                  | +Y |
   ----------------
   | -X | -Z | +X |
   ----------------
                  | -Y |
                  ------
                  | +Z |
                  ------
  */

  const int pixel_size = cubemap.num_of_channels * cubemap.bytes_per_channel;

  for (int face = 0; face < 6; ++face) {
    for (int j = 0; j < face_height; ++j) {
      for (int i = 0; i < face_width; ++i) {
        // NOLINTNEXTLINE(readability-identifier-length)
        int x = 0;
        // NOLINTNEXTLINE(readability-identifier-length)
        int y = 0;

        switch (face) {
          // GL_TEXTURE_CUBE_MAP_POSITIVE_X
        case 0:
          x = i;
          y = face_height + j;
          break;

          // GL_TEXTURE_CUBE_MAP_NEGATIVE_X
        case 1:
          x = 2 * face_width + i;
          y = 1 * face_height + j;
          break;

          // GL_TEXTURE_CUBE_MAP_POSITIVE_Y
        case 2:
          x = 2 * face_width - (i + 1);
          y = 1 * face_height - (j + 1);
          break;

          // GL_TEXTURE_CUBE_MAP_NEGATIVE_Y
        case 3:
          x = 2 * face_width - (i + 1);
          y = 3 * face_height - (j + 1);
          break;

          // GL_TEXTURE_CUBE_MAP_POSITIVE_Z
        case 4:
          x = 2 * face_width - (i + 1);
          y = bitmap.height - (j + 1);
          break;

          // GL_TEXTURE_CUBE_MAP_NEGATIVE_Z
        case 5:
          x = face_width + i;
          y = face_height + j;
          break;
        }

        // NOLINTNEXTLINE(bugprone-implicit-widening-of-multiplication-result,cppcoreguidelines-pro-bounds-pointer-arithmetic)
        memcpy(destination, source + (y * bitmap.width + x) * pixel_size,
               pixel_size);
        // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
        destination += pixel_size;
      }
    }
  }

  return cubemap;
}

} // namespace

auto create_image_and_memory(VkDevice device, VkPhysicalDevice physical_device,
                             uint32_t width, uint32_t height, VkFormat format,
                             VkImageTiling tiling, VkImageUsageFlags usage,
                             VkMemoryPropertyFlags properties,
                             VkImage &out_image,
                             VkDeviceMemory &out_image_memory,
                             VkImageCreateFlags flags) -> void {
  const VkImageCreateInfo image_info{
      .sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
      .flags = flags,
      .imageType = VK_IMAGE_TYPE_2D,
      .format = format,
      .extent = VkExtent3D{.width = width, .height = height, .depth = 1},
      .mipLevels = 1,
      .arrayLayers =
          (uint32_t)((flags == VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT) ? 6 : 1),
      .samples = VK_SAMPLE_COUNT_1_BIT,
      .tiling = tiling,
      .usage = usage,
      .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
      .queueFamilyIndexCount = 0,
      .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED};
  VK_CHECK(vkCreateImage(device, &image_info, nullptr, &out_image));

  VkMemoryRequirements mem_requirements;
  vkGetImageMemoryRequirements(device, out_image, &mem_requirements);
  const VkMemoryAllocateInfo allocate_info{
      .sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
      .allocationSize = mem_requirements.size,
      .memoryTypeIndex = find_memory_type(
          physical_device, mem_requirements.memoryTypeBits, properties)};
  VK_CHECK(
      vkAllocateMemory(device, &allocate_info, nullptr, &out_image_memory));

  vkBindImageMemory(device, out_image, out_image_memory, 0);
}

auto destroy_image_and_memory(VkDevice device, VkImage &out_image,
                              VkDeviceMemory &out_memory) -> void {
  vkDestroyImage(device, out_image, nullptr);
  vkFreeMemory(device, out_memory, nullptr);
}

auto create_image_from_data_pointer(const VulkanRenderDevice &render_device,
                                    VkImage &out_image,
                                    VkDeviceMemory &out_image_memory,
                                    void *data, uint32_t width, uint32_t height,
                                    VkFormat format, uint32_t layer_count,
                                    VkImageCreateFlags flags) -> void {
  create_image_and_memory(
      render_device.device, render_device.physical_device, width, height,
      format, VK_IMAGE_TILING_OPTIMAL,
      VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
      VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, out_image, out_image_memory, flags);

  copy_data_pointer_to_image(render_device, out_image, width, height, format,
                             layer_count, data);
}

auto create_image_from_file(const VulkanRenderDevice &render_device,
                            const char *file_name, VkImage &out_image,
                            VkDeviceMemory &image_memory) -> void {
  int width = 0;
  int height = 0;
  int num_of_channels = 0;
  stbi_uc *in_pixels =
      stbi_load(file_name, &width, &height, &num_of_channels, STBI_rgb_alpha);
  BL_CHECK(in_pixels == nullptr);

  create_image_from_data_pointer(render_device, out_image, image_memory,
                                 in_pixels, width, height,
                                 VK_FORMAT_R8G8B8A8_UNORM);

  stbi_image_free(in_pixels);
}

auto copy_data_pointer_to_image(const VulkanRenderDevice &render_device,
                                VkImage &out_image, uint32_t width,
                                uint32_t height, VkFormat format,
                                uint32_t layer_count, const void *data)
    -> void {
  const uint32_t bytes_per_pixel = bytes_per_texture_format(format);
  auto layer_size = static_cast<VkDeviceSize>(width) * height * bytes_per_pixel;
  const VkDeviceSize image_size = layer_size * layer_count;

  // Create staging buffer
  VkBuffer staging_buffer = nullptr;
  VkDeviceMemory staging_buffer_memory = nullptr;
  create_buffer_and_memory(render_device.device, render_device.physical_device,
                           image_size, VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
                           VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
                               VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
                           staging_buffer, staging_buffer_memory);

  // Copy data to staging buffer
  copy_data_pointer_to_buffer(render_device, staging_buffer_memory, 0, data,
                              image_size);

  // Convert out_image to updateable
  convert_image_layout(render_device, out_image, format,
                       VK_IMAGE_LAYOUT_UNDEFINED,
                       VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, layer_count);

  // Copy staging buffer to out_image
  copy_buffer_to_image(render_device, staging_buffer, out_image,
                       static_cast<uint32_t>(width),
                       static_cast<uint32_t>(height), layer_count);

  // Convert out_image from updateable to shader read-only
  convert_image_layout(render_device, out_image, format,
                       VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                       VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, layer_count);

  destroy_buffer_and_memory(render_device.device, staging_buffer,
                            staging_buffer_memory);
}

auto copy_buffer_to_image(const VulkanRenderDevice &render_device,
                          VkBuffer buffer, VkImage out_image, uint32_t width,
                          uint32_t height, uint32_t layer_count) -> void {
  VkCommandBuffer command_buffer =
      begin_single_use_command_buffer(render_device);

  const VkBufferImageCopy region{
      .bufferOffset = 0,
      .bufferRowLength = 0,
      .bufferImageHeight = 0,
      .imageSubresource =
          VkImageSubresourceLayers{.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
                                   .mipLevel = 0,
                                   .baseArrayLayer = 0,
                                   .layerCount = layer_count},
      .imageOffset = VkOffset3D{.x = 0, .y = 0, .z = 0},
      .imageExtent = VkExtent3D{.width = width, .height = height, .depth = 1}};
  vkCmdCopyBufferToImage(command_buffer, buffer, out_image,
                         VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &region);

  end_single_use_command_buffer(render_device, command_buffer);
}

auto convert_image_layout(const VulkanRenderDevice &render_device,
                          VkImage out_image, VkFormat format,
                          VkImageLayout old_layout, VkImageLayout new_layout,
                          uint32_t layer_count, uint32_t mip_levels) -> void {
  VkCommandBuffer command_buffer =
      begin_single_use_command_buffer(render_device);
  convert_image_layout_command(command_buffer, out_image, format, old_layout,
                               new_layout, layer_count, mip_levels);
  end_single_use_command_buffer(render_device, command_buffer);
}

auto create_cube_image_from_file(const VulkanRenderDevice &render_device,
                                 const char *file_name, VkImage &out_image,
                                 VkDeviceMemory &image_memory) -> void {
  int width = 0;
  int height = 0;
  int num_of_channels = 0;
  const auto *data =
      stbi_loadf(file_name, &width, &height, &num_of_channels, 3);
  BL_CHECK(data == nullptr);

  std::vector<float> img32(static_cast<size_t>(width * height * 4));
  float24to32(width, height, data, img32.data());

  // NOLINTNEXTLINE(cppcoreguidelines-pro-type-*)
  stbi_image_free(const_cast<void *>(reinterpret_cast<const void *>(data)));

  const Bitmap input(width, height, 4, img32.data());
  const Bitmap output = convertEquirectangularMapToVerticalCross(input);
  Bitmap cube = convertVerticalCrossToCubeMapFaces(output);

  create_image_from_data_pointer(render_device, out_image, image_memory,
                                 cube.data.data(), cube.width, cube.height,
                                 VK_FORMAT_R32G32B32A32_SFLOAT, 6,
                                 VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT);
}

auto create_image_view(VkDevice device, VkImage image, VkFormat format,
                       VkImageAspectFlags aspect_flags,
                       VkImageView &out_image_view, VkImageViewType view_type,
                       uint32_t layer_count) -> void {
  const VkImageViewCreateInfo view_info{
      .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
      .image = image,
      .viewType = view_type,
      .format = format,
      .subresourceRange = {.aspectMask = aspect_flags,
                           .baseMipLevel = 0,
                           .levelCount = 1,
                           .baseArrayLayer = 0,
                           .layerCount = layer_count}};
  VK_CHECK(vkCreateImageView(device, &view_info, nullptr, &out_image_view));
}

auto destroy_image_view(VkDevice device, VkImageView &out_image_view) -> void {
  vkDestroyImageView(device, out_image_view, nullptr);
}

auto create_texture_sampler(VkDevice device, VkSampler &out_sampler,
                            VkSamplerAddressMode address_mode) -> void {
  const VkSamplerCreateInfo sampler_info{
      .sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO,
      .magFilter = VK_FILTER_LINEAR,
      .minFilter = VK_FILTER_LINEAR,
      .mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR,
      .addressModeU = address_mode,
      .addressModeV = address_mode,
      .addressModeW = address_mode,
      .mipLodBias = 0.0F,
      .anisotropyEnable = VK_FALSE,
      .maxAnisotropy = 1,
      .compareEnable = VK_FALSE,
      .compareOp = VK_COMPARE_OP_ALWAYS,
      .minLod = 0.0F,
      .maxLod = 0.0F,
      .borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK,
      .unnormalizedCoordinates = VK_FALSE};
  VK_CHECK(vkCreateSampler(device, &sampler_info, nullptr, &out_sampler));
}

auto destroy_texture_sampler(VkDevice device, VkSampler &out_sampler) -> void {
  vkDestroySampler(device, out_sampler, nullptr);
}

auto create_graphics_pipeline(const VulkanRenderDevice &render_device,
                              VkRenderPass renderpass,
                              VkPipelineLayout pipeline_layout,
                              const std::vector<const char *> &shader_files,
                              VkPipeline &out_pipeline,
                              VkPrimitiveTopology topology, bool use_depth,
                              bool use_blending, bool dynamic_scissor_state,
                              uint32_t num_path_control_points) -> void {
  std::vector<VkShaderModule> shader_modules;
  std::vector<VkPipelineShaderStageCreateInfo> shader_stages;
  shader_stages.resize(shader_files.size());
  shader_modules.resize(shader_files.size());

  for (auto i = 0; i < shader_files.size(); i++) {
    const char *file_name = shader_files[i];
    VK_CHECK(create_shader_module(render_device.device, shader_modules[i],
                                  file_name));
    shader_stages[i] = VkPipelineShaderStageCreateInfo{
        .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
        .stage = get_shader_stage_flag(file_name),
        .module = shader_modules[i],
        .pName = "main"};
  }

  const VkPipelineVertexInputStateCreateInfo vertex_input_info{
      .sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO};
  const VkPipelineInputAssemblyStateCreateInfo input_assembly{
      .sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
      /* The only difference from createGraphicsPipeline() */
      .topology = topology,
      .primitiveRestartEnable = VK_FALSE};
  const VkViewport viewport{.x = 0.0F,
                            .y = 0.0F,
                            .width = static_cast<float>(SCREEN_WIDTH),
                            .height = static_cast<float>(SCREEN_HEIGHT),
                            .minDepth = 0.0F,
                            .maxDepth = 1.0F};
  const VkRect2D scissor{.offset = {0, 0},
                         .extent = {SCREEN_WIDTH, SCREEN_HEIGHT}};
  const VkPipelineViewportStateCreateInfo viewport_state{
      .sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
      .viewportCount = 1,
      .pViewports = &viewport,
      .scissorCount = 1,
      .pScissors = &scissor};
  const VkPipelineRasterizationStateCreateInfo rasterizer{
      .sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
      .polygonMode = VK_POLYGON_MODE_FILL,
      .cullMode = VK_CULL_MODE_NONE,
      .frontFace = VK_FRONT_FACE_CLOCKWISE,
      .lineWidth = 1.0F};
  const VkPipelineMultisampleStateCreateInfo multisampling{
      .sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
      .rasterizationSamples = VK_SAMPLE_COUNT_1_BIT,
      .sampleShadingEnable = VK_FALSE,
      .minSampleShading = 1.0F};
  const VkPipelineColorBlendAttachmentState color_blend_attachment{
      .blendEnable = VK_TRUE,
      .srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA,
      .dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA,
      .colorBlendOp = VK_BLEND_OP_ADD,
      .srcAlphaBlendFactor = use_blending ? VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA
                                          : VK_BLEND_FACTOR_ONE,
      .dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO,
      .alphaBlendOp = VK_BLEND_OP_ADD,
      .colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT |
                        VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT};
  const VkPipelineColorBlendStateCreateInfo color_blending{
      .sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
      .logicOpEnable = VK_FALSE,
      .logicOp = VK_LOGIC_OP_COPY,
      .attachmentCount = 1,
      .pAttachments = &color_blend_attachment,
      .blendConstants = {0.0F, 0.0F, 0.0F, 0.0F}};
  const VkPipelineDepthStencilStateCreateInfo depth_stencil{
      .sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO,
      .depthTestEnable = static_cast<VkBool32>(use_depth ? VK_TRUE : VK_FALSE),
      .depthWriteEnable = static_cast<VkBool32>(use_depth ? VK_TRUE : VK_FALSE),
      .depthCompareOp = VK_COMPARE_OP_LESS,
      .depthBoundsTestEnable = VK_FALSE,
      .minDepthBounds = 0.0F,
      .maxDepthBounds = 1.0F};
  const VkDynamicState dynamic_state_elt = VK_DYNAMIC_STATE_SCISSOR;
  const VkPipelineDynamicStateCreateInfo dynamic_state{
      .sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO,
      .dynamicStateCount = 1,
      .pDynamicStates = &dynamic_state_elt};
  const VkPipelineTessellationStateCreateInfo tessellation_state{
      .sType = VK_STRUCTURE_TYPE_PIPELINE_TESSELLATION_STATE_CREATE_INFO,
      .patchControlPoints = num_path_control_points};
  const VkGraphicsPipelineCreateInfo pipeline_info{
      .sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
      .stageCount = static_cast<uint32_t>(shader_stages.size()),
      .pStages = shader_stages.data(),
      .pVertexInputState = &vertex_input_info,
      .pInputAssemblyState = &input_assembly,
      .pTessellationState = (topology == VK_PRIMITIVE_TOPOLOGY_PATCH_LIST)
                                ? &tessellation_state
                                : nullptr,
      .pViewportState = &viewport_state,
      .pRasterizationState = &rasterizer,
      .pMultisampleState = &multisampling,
      .pDepthStencilState = use_depth ? &depth_stencil : nullptr,
      .pColorBlendState = &color_blending,
      .pDynamicState = dynamic_scissor_state ? &dynamic_state : nullptr,
      .layout = pipeline_layout,
      .renderPass = renderpass,
      .subpass = 0,
      .basePipelineHandle = VK_NULL_HANDLE,
      .basePipelineIndex = -1};
  VK_CHECK(vkCreateGraphicsPipelines(render_device.device, VK_NULL_HANDLE, 1,
                                     &pipeline_info, nullptr, &out_pipeline));

  for (auto *module : shader_modules) {
    vkDestroyShaderModule(render_device.device, module, nullptr);
  }
}

auto destroy_graphics_pipeline(VkDevice device, VkPipeline &out_pipeline)
    -> void {
  vkDestroyPipeline(device, out_pipeline, nullptr);
}

auto create_pipeline_layout(VkDevice device,
                            VkDescriptorSetLayout descriptor_set_layout,
                            VkPipelineLayout &out_pipeline_layout) -> void {
  const VkPipelineLayoutCreateInfo pipeline_layout_info{
      .sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
      .setLayoutCount = 1,
      .pSetLayouts = &descriptor_set_layout,
      .pushConstantRangeCount = 0};
  VK_CHECK(vkCreatePipelineLayout(device, &pipeline_layout_info, nullptr,
                                  &out_pipeline_layout));
}

auto destroy_pipeline_layout(VkDevice device,
                             VkPipelineLayout &out_pipeline_layout) -> void {
  vkDestroyPipelineLayout(device, out_pipeline_layout, nullptr);
}

auto create_buffer_and_memory(VkDevice device, VkPhysicalDevice physical_device,
                              VkDeviceSize size, VkBufferUsageFlags usage,
                              VkMemoryPropertyFlags properties,
                              VkBuffer &out_buffer,
                              VkDeviceMemory &out_buffer_memory) -> void {
  const VkBufferCreateInfo buffer_info{.sType =
                                           VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
                                       .size = size,
                                       .usage = usage,
                                       .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
                                       .queueFamilyIndexCount = 0};
  VK_CHECK(vkCreateBuffer(device, &buffer_info, nullptr, &out_buffer));

  VkMemoryRequirements mem_requirements;
  vkGetBufferMemoryRequirements(device, out_buffer, &mem_requirements);
  const VkMemoryAllocateInfo allocate_info{
      .sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
      .allocationSize = mem_requirements.size,
      .memoryTypeIndex = find_memory_type(
          physical_device, mem_requirements.memoryTypeBits, properties)};
  VK_CHECK(
      vkAllocateMemory(device, &allocate_info, nullptr, &out_buffer_memory));

  vkBindBufferMemory(device, out_buffer, out_buffer_memory, 0);
}

auto destroy_buffer_and_memory(VkDevice device, VkBuffer &out_buffer,
                               VkDeviceMemory &out_memory) -> void {
  vkDestroyBuffer(device, out_buffer, nullptr);
  vkFreeMemory(device, out_memory, nullptr);
}

auto copy_buffer_to_buffer(const VulkanRenderDevice &render_device,
                           VkBuffer buffer, VkBuffer out_buffer,
                           VkDeviceSize size) -> void {
  VkCommandBuffer command_buffer =
      begin_single_use_command_buffer(render_device);
  const VkBufferCopy copy_region{.srcOffset = 0, .dstOffset = 0, .size = size};
  vkCmdCopyBuffer(command_buffer, buffer, out_buffer, 1, &copy_region);
  end_single_use_command_buffer(render_device, command_buffer);
}

auto copy_data_pointer_to_buffer(const VulkanRenderDevice &render_device,
                                 const VkDeviceMemory &out_buffer_memory,
                                 VkDeviceSize device_offset, const void *data,
                                 const size_t data_size) -> void {
  void *mapped_out_buffer_memory = nullptr;
  vkMapMemory(render_device.device, out_buffer_memory, device_offset, data_size,
              0, &mapped_out_buffer_memory);
  memcpy(mapped_out_buffer_memory, data, data_size);
  vkUnmapMemory(render_device.device, out_buffer_memory);
}

auto create_vertices_buffer_from_data_pointer(
    const VulkanRenderDevice &render_device, const void *vertices_data,
    size_t vertices_data_size, const void *indices_data,
    size_t indices_data_size, VkBuffer &out_vertices_buffer,
    VkDeviceMemory &out_vertices_buffer_memory) -> void {
  const VkDeviceSize buffer_size = vertices_data_size + indices_data_size;

  // Create staging buffer
  VkBuffer staging_buffer{};
  VkDeviceMemory staging_buffer_memory{};
  create_buffer_and_memory(render_device.device, render_device.physical_device,
                           buffer_size, VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
                           VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
                               VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
                           staging_buffer, staging_buffer_memory);

  // Copy vertices data and indices data to staging buffer
  void *staging_buffer_mapped = nullptr;
  vkMapMemory(render_device.device, staging_buffer_memory, 0, buffer_size, 0,
              &staging_buffer_mapped);
  memcpy(staging_buffer_mapped, vertices_data, vertices_data_size);
  // NOLINTNEXTLINE
  memcpy(reinterpret_cast<uint8_t *>(staging_buffer_mapped) +
             vertices_data_size,
         indices_data, indices_data_size);
  vkUnmapMemory(render_device.device, staging_buffer_memory);

  // Create vertices buffer
  create_buffer_and_memory(
      render_device.device, render_device.physical_device, buffer_size,
      VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
      VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, out_vertices_buffer,
      out_vertices_buffer_memory);

  // Copy staging buffer to vertices buffer
  copy_buffer_to_buffer(render_device, staging_buffer, out_vertices_buffer,
                        buffer_size);

  destroy_buffer_and_memory(render_device.device, staging_buffer,
                            staging_buffer_memory);
}

// Create color and depth subpasses
auto create_renderpass(const VulkanRenderDevice &render_device,
                       VkRenderPass &out_renderpass,
                       const RenderPassCreateInfo &create_info) -> void {
  const bool first = (create_info.flags & RenderPassBit_First) != 0;
  const bool last = (create_info.flags & RenderPassBit_Last) != 0;

  const VkAttachmentDescription color_attachment{
      .format = VK_FORMAT_B8G8R8A8_UNORM,
      .samples = VK_SAMPLE_COUNT_1_BIT,
      .loadOp = create_info.clear_color ? VK_ATTACHMENT_LOAD_OP_CLEAR
                                        : VK_ATTACHMENT_LOAD_OP_LOAD,
      .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
      .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
      .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
      // VK_IMAGE_LAYOUT_UNDEFINED --> VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
      // --> VK_IMAGE_LAYOUT_PRESENT_SRC_KHR
      .initialLayout = first ? VK_IMAGE_LAYOUT_UNDEFINED
                             : VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
      .finalLayout = !last ? VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
                           : VK_IMAGE_LAYOUT_PRESENT_SRC_KHR};
  const VkAttachmentDescription depth_attachment{
      .format = find_depth_format(render_device.physical_device),
      .samples = VK_SAMPLE_COUNT_1_BIT,
      .loadOp = create_info.clear_depth ? VK_ATTACHMENT_LOAD_OP_CLEAR
                                        : VK_ATTACHMENT_LOAD_OP_LOAD,
      .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
      .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
      .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
      .initialLayout = create_info.clear_depth
                           ? VK_IMAGE_LAYOUT_UNDEFINED
                           : VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
      .finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL};
  std::vector<VkSubpassDependency> dependencies{
      {.srcSubpass = VK_SUBPASS_EXTERNAL,
       .dstSubpass = 0,
       .srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
       .dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
       .srcAccessMask = 0,
       .dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT |
                        VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT}};
  const VkAttachmentReference color_attachment_reference{
      .attachment = 0, .layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL};
  const VkAttachmentReference depth_attachment_reference{
      .attachment = 1,
      .layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL};
  const VkSubpassDescription subpass{
      .pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS,
      .inputAttachmentCount = 0,
      .colorAttachmentCount = 1,
      .pColorAttachments = &color_attachment_reference,
      .pDepthStencilAttachment = &depth_attachment_reference,
      .preserveAttachmentCount = 0};
  std::array<VkAttachmentDescription, 2> attachments{color_attachment,
                                                     depth_attachment};
  const VkRenderPassCreateInfo renderpass_create_info{
      .sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
      .attachmentCount = 2,
      .pAttachments = attachments.data(),
      .subpassCount = 1,
      .pSubpasses = &subpass,
      .dependencyCount = static_cast<uint32_t>(dependencies.size()),
      .pDependencies = dependencies.data()};
  VK_CHECK(vkCreateRenderPass(render_device.device, &renderpass_create_info,
                              nullptr, &out_renderpass));
}

auto destroy_renderpass(VkDevice device, VkRenderPass &out_renderpass) -> void {
  vkDestroyRenderPass(device, out_renderpass, nullptr);
}

auto create_descriptor_pool(const VulkanRenderDevice &render_device,
                            uint32_t uniform_buffer_count,
                            uint32_t storage_buffer_count,
                            uint32_t sampler_count,
                            VkDescriptorPool &out_descriptor_pool) -> void {
  const auto image_count =
      static_cast<uint32_t>(render_device.swapchain_images.size());

  std::vector<VkDescriptorPoolSize> pool_sizes{};

  if (uniform_buffer_count != 0U)
    pool_sizes.push_back(VkDescriptorPoolSize{
        .type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
        .descriptorCount = image_count * uniform_buffer_count});

  if (storage_buffer_count != 0U)
    pool_sizes.push_back(VkDescriptorPoolSize{
        .type = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
        .descriptorCount = image_count * storage_buffer_count});

  if (sampler_count != 0U)
    pool_sizes.push_back(
        VkDescriptorPoolSize{.type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
                             .descriptorCount = image_count * sampler_count});

  const VkDescriptorPoolCreateInfo pool_info{
      .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
      .maxSets = static_cast<uint32_t>(image_count),
      .poolSizeCount = static_cast<uint32_t>(pool_sizes.size()),
      .pPoolSizes = pool_sizes.empty() ? nullptr : pool_sizes.data()};
  VK_CHECK(vkCreateDescriptorPool(render_device.device, &pool_info, nullptr,
                                  &out_descriptor_pool));
}

auto destroy_descriptor_pool(VkDevice device,
                             VkDescriptorPool &out_descriptor_pool) -> void {
  vkDestroyDescriptorPool(device, out_descriptor_pool, nullptr);
}

auto allocate_descriptor_sets(
    const VulkanRenderDevice &render_device,
    const VkDescriptorPool &descriptor_pool,
    const VkDescriptorSetLayout &descriptor_set_layout,
    std::vector<VkDescriptorSet> &out_descriptor_sets) -> void {
  out_descriptor_sets.resize(render_device.swapchain_images.size());

  std::vector<VkDescriptorSetLayout> layouts(
      render_device.swapchain_images.size(), descriptor_set_layout);
  const VkDescriptorSetAllocateInfo allocate_info{
      .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
      .descriptorPool = descriptor_pool,
      .descriptorSetCount =
          static_cast<uint32_t>(render_device.swapchain_images.size()),
      .pSetLayouts = layouts.data()};
  VK_CHECK(vkAllocateDescriptorSets(render_device.device, &allocate_info,
                                    out_descriptor_sets.data()));
}

auto destroy_descriptor_set_layout(
    VkDevice device, VkDescriptorSetLayout &out_descriptor_set_layout) -> void {
  vkDestroyDescriptorSetLayout(device, out_descriptor_set_layout, nullptr);
}

// Create color and depth framebuffers
auto create_framebuffers(const VulkanRenderDevice &render_device,
                         VkRenderPass renderpass,
                         std::vector<VkFramebuffer> &out_swapchain_framebuffers)
    -> void {
  out_swapchain_framebuffers.resize(render_device.swapchain_image_views.size());

  for (auto i = 0; i < render_device.swapchain_images.size(); i++) {
    std::array<VkImageView, 2> attachments{
        render_device.swapchain_image_views[i],
        render_device.depth_texture.image_view};
    const VkFramebufferCreateInfo framebuffer_create_info{
        .sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
        .renderPass = renderpass,
        .attachmentCount = static_cast<uint32_t>(
            (render_device.depth_texture.image_view == VK_NULL_HANDLE) ? 1 : 2),
        .pAttachments = attachments.data(),
        .width = SCREEN_WIDTH,
        .height = SCREEN_HEIGHT,
        .layers = 1};
    VK_CHECK(vkCreateFramebuffer(render_device.device, &framebuffer_create_info,
                                 nullptr, &out_swapchain_framebuffers[i]));
  }
}

auto destroy_framebuffer(VkDevice device, VkFramebuffer &out_framebuffer)
    -> void {
  vkDestroyFramebuffer(device, out_framebuffer, nullptr);
}

auto create_command_buffers(VkDevice device, VkCommandPool command_pool,
                            uint32_t image_count,
                            std::vector<VkCommandBuffer> &out_command_buffers)
    -> void {
  out_command_buffers.resize(image_count);
  const VkCommandBufferAllocateInfo allocate_info{
      .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
      .commandPool = command_pool,
      .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
      .commandBufferCount = image_count};
  VK_CHECK(vkAllocateCommandBuffers(device, &allocate_info,
                                    out_command_buffers.data()));
}

auto begin_command_buffer(VulkanRenderDevice &render_device,
                          uint32_t image_index) -> void {
  VK_CHECK(vkResetCommandBuffer(render_device.command_buffers[image_index], 0));
  const VkCommandBufferBeginInfo begin_info{
      .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
      .flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT};
  VK_CHECK(vkBeginCommandBuffer(render_device.command_buffers[image_index],
                                &begin_info));
}

auto end_command_buffer(VulkanRenderDevice &render_device, uint32_t image_index)
    -> void {
  VK_CHECK(vkEndCommandBuffer(render_device.command_buffers[image_index]));
}

auto submit_commandbuffer(VulkanRenderDevice &render_device,
                          uint32_t image_index) -> void {
  constexpr std::array<VkPipelineStageFlags, 1> wait_stages{
      VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};
  const VkSubmitInfo submit_info{
      .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
      .waitSemaphoreCount = 1,
      .pWaitSemaphores = &render_device.image_acquired_semaphore,
      .pWaitDstStageMask = wait_stages.data(),
      .commandBufferCount = 1,
      .pCommandBuffers = &render_device.command_buffers[image_index],
      .signalSemaphoreCount = 1,
      .pSignalSemaphores = &render_device.submit_complete_semaphore};
  VK_CHECK(vkQueueSubmit(render_device.graphics_queue, 1, &submit_info,
                         render_device.submit_complete_fence));
}

auto begin_single_use_command_buffer(const VulkanRenderDevice &render_device)
    -> VkCommandBuffer {
  VkCommandBuffer command_buffer = nullptr;

  const VkCommandBufferAllocateInfo allocate_info{
      .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
      .commandPool = render_device.command_pool,
      .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
      .commandBufferCount = 1};
  vkAllocateCommandBuffers(render_device.device, &allocate_info,
                           &command_buffer);

  const VkCommandBufferBeginInfo begin_info{
      .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
      .flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT};
  vkBeginCommandBuffer(command_buffer, &begin_info);

  return command_buffer;
}

auto end_single_use_command_buffer(const VulkanRenderDevice &render_device,
                                   VkCommandBuffer command_buffer) -> void {
  vkEndCommandBuffer(command_buffer);

  const VkSubmitInfo submit_info{.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
                                 .waitSemaphoreCount = 0,
                                 .commandBufferCount = 1,
                                 .pCommandBuffers = &command_buffer,
                                 .signalSemaphoreCount = 0};

  vkQueueSubmit(render_device.graphics_queue, 1, &submit_info, VK_NULL_HANDLE);
  vkQueueWaitIdle(render_device.graphics_queue);

  vkFreeCommandBuffers(render_device.device, render_device.command_pool, 1,
                       &command_buffer);
}

auto create_semaphore(VkDevice device, VkSemaphore &out_semaphore) -> void {
  const VkSemaphoreCreateInfo create_info{
      .sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO};
  VK_CHECK(vkCreateSemaphore(device, &create_info, nullptr, &out_semaphore));
}

auto create_fence(VkDevice device, VkFence &out_fence) -> void {
  const VkFenceCreateInfo create_info{.sType =
                                          VK_STRUCTURE_TYPE_FENCE_CREATE_INFO,
                                      .flags = VK_FENCE_CREATE_SIGNALED_BIT};
  VK_CHECK(vkCreateFence(device, &create_info, nullptr, &out_fence));
}

auto wait_on_fence(VulkanRenderDevice &render_device, VkFence fence) -> void {
  vkWaitForFences(render_device.device, 1, &fence, VK_TRUE, UINT64_MAX);
  vkResetFences(render_device.device, 1, &fence);
}

auto create_vulkan_texture_from_file(const VulkanRenderDevice &render_device,
                                     const char *file_path,
                                     VulkanTexture &out_texture) -> void {
  create_image_from_file(render_device, file_path,
                         out_texture.vulkan_image.image,
                         out_texture.vulkan_image.image_memory);
  create_image_view(render_device.device, out_texture.vulkan_image.image,
                    VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_ASPECT_COLOR_BIT,
                    out_texture.vulkan_image.image_view);
  create_texture_sampler(render_device.device, out_texture.sampler);
}

auto create_cube_vulkan_texture_from_file(
    const VulkanRenderDevice &render_device, const char *file_path,
    VulkanTexture &out_texture) -> void {
  create_cube_image_from_file(render_device, file_path,
                              out_texture.vulkan_image.image,
                              out_texture.vulkan_image.image_memory);
  create_image_view(render_device.device, out_texture.vulkan_image.image,
                    VK_FORMAT_R32G32B32A32_SFLOAT, VK_IMAGE_ASPECT_COLOR_BIT,
                    out_texture.vulkan_image.image_view,
                    VK_IMAGE_VIEW_TYPE_CUBE, 6);
  create_texture_sampler(render_device.device, out_texture.sampler,
                         VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE);
}

auto create_ktx_vulkan_texture_from_file(
    const VulkanRenderDevice &render_device, const char *file_path,
    VulkanTexture &out_texture) -> void {
  auto gli_texture = gli::load_ktx(file_path);
  auto gli_extent = gli_texture.extent(0);
  create_image_from_data_pointer(
      render_device, out_texture.vulkan_image.image,
      out_texture.vulkan_image.image_memory,
      // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
      reinterpret_cast<uint8_t *>(gli_texture.data(0, 0, 0)),
      // NOLINTNEXTLINE(cppcoreguidelines-pro-type-union-access)
      gli_extent.x, gli_extent.y, VK_FORMAT_R16G16_SFLOAT);
  create_image_view(render_device.device, out_texture.vulkan_image.image,
                    VK_FORMAT_R16G16_SFLOAT, VK_IMAGE_ASPECT_COLOR_BIT,
                    out_texture.vulkan_image.image_view);
  create_texture_sampler(render_device.device, out_texture.sampler,
                         VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE);
}

auto destroy_vulkan_texture(VkDevice device, VulkanTexture &texture) -> void {
  destroy_vulkan_image(device, texture.vulkan_image);
  destroy_texture_sampler(device, texture.sampler);
}

auto create_vulkan_image(VkDevice device, VkPhysicalDevice physical_device,
                         uint32_t width, uint32_t height, VkFormat depth_format,
                         VulkanImage &out_vulkan_image) -> void {
  // Create image and memory
  vulkanutils::create_image_and_memory(
      device, physical_device, width, height, depth_format,
      VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
      VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, out_vulkan_image.image,
      out_vulkan_image.image_memory);

  // Create image view
  vulkanutils::create_image_view(device, out_vulkan_image.image, depth_format,
                                 VK_IMAGE_ASPECT_DEPTH_BIT,
                                 out_vulkan_image.image_view);
}

auto destroy_vulkan_image(VkDevice device, VulkanImage &out_image) -> void {
  vkDestroyImageView(device, out_image.image_view, nullptr);
  vkDestroyImage(device, out_image.image, nullptr);
  vkFreeMemory(device, out_image.image_memory, nullptr);
}

auto create_uniform_buffers(
    const VulkanRenderDevice &render_device,
    std::vector<VkBuffer> &out_uniform_buffers,
    std::vector<VkDeviceMemory> &out_uniform_buffers_memory) -> void {
  constexpr VkDeviceSize BUFFER_SIZE = sizeof(UniformBuffer);

  out_uniform_buffers.resize(render_device.swapchain_images.size());
  out_uniform_buffers_memory.resize(render_device.swapchain_images.size());

  for (auto i = 0; i < render_device.swapchain_images.size(); i++) {
    create_buffer_and_memory(
        render_device.device, render_device.physical_device, BUFFER_SIZE,
        VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
            VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
        out_uniform_buffers[i], out_uniform_buffers_memory[i]);
  }
}

auto update_uniform_buffer_memory_from_data_pointer(
    const VulkanRenderDevice &render_device, const size_t image_index,
    const void *data, const size_t data_size,
    std::vector<VkDeviceMemory> &out_uniform_buffers_memory) -> void {
  void *uniform_buffer_mapped = nullptr;
  vkMapMemory(render_device.device, out_uniform_buffers_memory[image_index], 0,
              data_size, 0, &uniform_buffer_mapped);
  memcpy(uniform_buffer_mapped, data, data_size);
  vkUnmapMemory(render_device.device, out_uniform_buffers_memory[image_index]);
}

auto find_depth_format(VkPhysicalDevice device) -> VkFormat {
  return find_supported_format(
      device,
      {VK_FORMAT_D32_SFLOAT, VK_FORMAT_D32_SFLOAT_S8_UINT,
       VK_FORMAT_D24_UNORM_S8_UINT},
      VK_IMAGE_TILING_OPTIMAL, VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT);
}

} // namespace vulkanutils
