#include "FinishRenderPass.h"

#include "vulkanutils.h"

auto FinishRenderPass::update_uniform_buffer(
    mat4 view, mat4 project, vec3 camera_position_vector,
    const VulkanRenderDevice &render_device, uint32_t image_index) -> void {}

auto FinishRenderPass::fill_command_buffer(
    const VulkanRenderDevice &render_device, uint32_t image_index) -> void {
  const VkRect2D screen_rect{
      .offset = {0, 0},
      .extent = {.width = SCREEN_WIDTH, .height = SCREEN_HEIGHT}};
  const VkRenderPassBeginInfo renderpass_info{
      .sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
      .renderPass = renderpass,
      .framebuffer = swapchain_framebuffers[image_index],
      .renderArea = screen_rect};
  vkCmdBeginRenderPass(render_device.command_buffers[image_index],
                       &renderpass_info, VK_SUBPASS_CONTENTS_INLINE);
  vkCmdEndRenderPass(render_device.command_buffers[image_index]);
}

FinishRenderPass::FinishRenderPass(VulkanRenderDevice &render_device)
    : device_reference(render_device.device) {
  vulkanutils::create_renderpass(render_device, renderpass,
                                 vulkanutils::RenderPassCreateInfo{
                                     .clear_color = false,
                                     .clear_depth = false,
                                     .flags = vulkanutils::RenderPassBit_Last});
  vulkanutils::create_framebuffers(render_device, renderpass,
                                   swapchain_framebuffers);
}

FinishRenderPass::~FinishRenderPass() {
  for (auto *framebuffer : swapchain_framebuffers) {
    vulkanutils::destroy_framebuffer(device_reference, framebuffer);
  }
  vulkanutils::destroy_renderpass(device_reference, renderpass);
}
