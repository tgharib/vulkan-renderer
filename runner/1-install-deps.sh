#!/usr/bin/env bash

pikaur -S xmake conan vcpkg
# Manually install LunarG SDK (https://vulkan.lunarg.com/doc/sdk/1.3.236.0/linux/getting_started.html) or use a shortcut (https://doc.magnum.graphics/magnum/platforms-vk.html#platforms-vk-sdk)
