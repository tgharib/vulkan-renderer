#!/usr/bin/env bash
if [[ "$#" -ne 2 ]]; then
  echo "Usage: 2-setup-project.sh [toolchain] [mode]"
  echo "Toolchains: clang, gcc (xmake show -l toolchains)"
  echo "Modes: debug, undefined, memory, thread, valgrind, profile, release"
  exit 1
fi
rm -rf build/ .xmake/ compile_commands.json
export CONAN_CPU_COUNT=$(($(nproc)+2)) # https://docs.conan.io/en/1.6/reference/config_files/conan.conf.html
export VCPKG_ROOT=/opt/vcpkg
export VCPKG_DOWNLOADS=/var/cache/vcpkg
xmake f --toolchain=$1 -y -m $2
xmake project -k compile_commands
