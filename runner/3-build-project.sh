#!/usr/bin/env bash

clear
rm -rf shaders
mkdir shaders
glslc src/shaders/Cubemap.vert -o shaders/Cubemap.vert
glslc src/shaders/Cubemap.frag -o shaders/Cubemap.frag
glslc src/shaders/Model.vert -o shaders/Model.vert
glslc src/shaders/Model.frag -I . -o shaders/Model.frag
xmake
